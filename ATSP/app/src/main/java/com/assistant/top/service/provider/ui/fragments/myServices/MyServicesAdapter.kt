package com.assistant.top.service.provider.ui.fragments.myServices

import android.annotation.SuppressLint
import android.content.Context
import android.os.SystemClock
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.assistant.top.service.provider.R
import com.assistant.top.service.provider.databinding.AddServiceItemListBinding
import com.assistant.top.service.provider.databinding.SubAddMyServiceItemBinding
import com.assistant.top.service.provider.extension.gone
import com.assistant.top.service.provider.extension.visible
import com.assistant.top.service.provider.utility.Constants

class MyServicesAdapter : RecyclerView.Adapter<MyServicesAdapter.MenuHolder>() {

    private var selectedPosition = -1
    lateinit var context: Context
    lateinit var binding: AddServiceItemListBinding

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MenuHolder {

        context = parent.context
        binding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.add_service_item_list,
            parent,

            false
        )

        return MenuHolder(binding)
    }

    @SuppressLint("NotifyDataSetChanged")
    override fun onBindViewHolder(holder: MenuHolder, @SuppressLint("RecyclerView") position: Int) {

        if (selectedPosition == position) {
            Constants.onClickView(holder.binding.rvSubIServices, holder.binding.upDownArrow)
            holder.binding.rvSubIServices.adapter = SubMyServicesAdapter()
        } else {
            holder.binding.upDownArrow.setImageResource(R.drawable.ic_arrow_down)
            holder.binding.rvSubIServices.gone()
        }

        when (position) {
            0 -> {
                setData(holder, "House", R.drawable.ic_doctor)
            }
            1 -> {
                setData(holder, "Construction", R.drawable.ic_doctor)
            }
            2 -> {
                setData(holder, "Auto Mechanic", R.drawable.ic_doctor)
            }
            3 -> {
                setData(holder, "Fitness Club", R.drawable.ic_doctor)
            }
            4 -> {
                setData(holder, "Doctors", R.drawable.ic_doctor)
            }
        }

        holder.itemView.setOnClickListener {
            selectedPosition = position
            notifyDataSetChanged()
        }

    }

    private fun setData(holder: MenuHolder, serviceName: String, resource: Int) {

        holder.binding.serviceIcon.setImageResource(resource)
        holder.binding.serviceName.text = serviceName
    }

    override fun getItemCount(): Int {
        return 5
    }

    class MenuHolder(var binding: AddServiceItemListBinding) : RecyclerView.ViewHolder(binding.root)

    class SubMyServicesAdapter : RecyclerView.Adapter<SubMyServicesAdapter.MenuHolder>() {

        private var selectedPosition = -1
        lateinit var context: Context
        lateinit var binding: SubAddMyServiceItemBinding

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MenuHolder {

            context = parent.context
            binding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.sub_add_my_service_item,
                parent,

                false
            )

            return MenuHolder(binding)
        }

        @SuppressLint("NotifyDataSetChanged")
        override fun onBindViewHolder(
            holder: MenuHolder,
            @SuppressLint("RecyclerView") position: Int
        ) {

            if (selectedPosition == position) {
                Constants.onClickView(holder.binding.rvMiniSubIServices, holder.binding.upDownArrow)
                holder.binding.rvMiniSubIServices.adapter = MiniSubMyServicesAdapter()
            } else {
                holder.binding.upDownArrow.setImageResource(R.drawable.ic_arrow_down)
                holder.binding.rvMiniSubIServices.gone()
            }

            when (position) {
                0 -> {
                    setData(holder, "Cement Delivery")
                }
                1 -> {
                    setData(holder, "Sofa Cleaning")
                }
                2 -> {
                    setData(holder, "Room Floor Cleaning")
                }
                3 -> {
                    setData(holder, "Sofa Cleaning")
                }
            }

            holder.itemView.setOnClickListener {
                selectedPosition = position
                notifyDataSetChanged()
            }

        }

        private fun setData(holder: MenuHolder, serviceName: String) {

            holder.binding.subServiceName.text = serviceName
        }

        override fun getItemCount(): Int {
            return 4
        }

        class MenuHolder(var binding: SubAddMyServiceItemBinding) :
            RecyclerView.ViewHolder(binding.root)


        class MiniSubMyServicesAdapter :
            RecyclerView.Adapter<MiniSubMyServicesAdapter.MenuHolder>() {

            lateinit var context: Context
            lateinit var binding: SubAddMyServiceItemBinding

            override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MenuHolder {

                context = parent.context
                binding = DataBindingUtil.inflate(
                    LayoutInflater.from(parent.context),
                    R.layout.sub_add_my_service_item,
                    parent,

                    false
                )

                return MenuHolder(binding)
            }

            @SuppressLint("NotifyDataSetChanged")
            override fun onBindViewHolder(
                holder: MenuHolder,
                @SuppressLint("RecyclerView") position: Int
            ) {

                holder.binding.upDownArrow.gone()
                holder.binding.layValues.visible()

                when (position) {
                    0 -> {
                        setData(holder, "JK")
                    }
                    1 -> {
                        setData(holder, "Zebra")
                    }
                }


            }

            private fun setData(holder: MenuHolder, serviceName: String) {

                holder.binding.subServiceName.text = serviceName
            }

            override fun getItemCount(): Int {
                return 2
            }

            class MenuHolder(var binding: SubAddMyServiceItemBinding) :
                RecyclerView.ViewHolder(binding.root)
        }
    }

}