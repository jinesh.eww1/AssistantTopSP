package com.assistant.top.service.provider.ui.service_details

import android.app.Dialog
import android.os.Bundle
import android.os.SystemClock
import android.util.Log
import android.view.LayoutInflater
import android.view.Window
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContentProviderCompat.requireContext
import androidx.core.content.ContextCompat
import androidx.core.graphics.drawable.toBitmap
import com.assistant.top.service.provider.BR
import com.assistant.top.service.provider.R
import com.assistant.top.service.provider.base.BaseActivity
import com.assistant.top.service.provider.databinding.ActivityServiceDetailsBinding
import com.assistant.top.service.provider.databinding.DialogServiceCompletedBinding
import com.assistant.top.service.provider.databinding.DialogueFeedbackBinding
import com.assistant.top.service.provider.extension.startNewActivity
import com.assistant.top.service.provider.extension.visible
import com.assistant.top.service.provider.ui.chat.ChatActivity
import com.assistant.top.service.provider.utility.Constants
import com.assistant.top.service.provider.utility.IntentKey
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import dagger.hilt.android.AndroidEntryPoint


@AndroidEntryPoint
class ServiceDetailsActivity :
    BaseActivity<ActivityServiceDetailsBinding, ServiceDetailstViewModel>(),
    ServiceDetailsNavigator,
    OnMapReadyCallback {

    private var position: Int = 0
    override val layoutId: Int get() = R.layout.activity_service_details
    override val bindingVariable: Int get() = BR.viewmodel
    private var mMap: GoogleMap? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mViewModel.setNavigator(this)
        initt()
        clickListener()
        mapInitialize()
        binding.btnSendMessage.setOnClickListener {
            startNewActivity(ChatActivity::class.java)
        }

    }

    private fun mapInitialize() {

        val mapFragment = supportFragmentManager.findFragmentById(R.id.mapView) as MapFragment?
        mapFragment!!.getMapAsync(this)
    }

    private fun clickListener() {
        binding.btnComplete.setCommonButtonListener {
            if (SystemClock.elapsedRealtime() - Constants.mLastClickTime < 1000) {
                return@setCommonButtonListener
            }
            Constants.mLastClickTime = SystemClock.elapsedRealtime()
            openDialogCompleted()
        }

        binding.btnStartService.setCommonButtonListener {
            if (SystemClock.elapsedRealtime() - Constants.mLastClickTime < 1000) {
                return@setCommonButtonListener
            }
            Constants.mLastClickTime = SystemClock.elapsedRealtime()
            openDialogStartService()
        }
        binding.btnAccept.setCommonButtonListener {
            if (SystemClock.elapsedRealtime() - Constants.mLastClickTime < 1000) {
                return@setCommonButtonListener
            }
            Constants.mLastClickTime = SystemClock.elapsedRealtime()
            openDialogRequestedAccepted()
        }
    }

    private fun openDialogRequestedAccepted() {

        val dialog = Dialog(this, R.style.RoundedCornersDialogRating)
        var dialoginding = DialogServiceCompletedBinding.inflate(LayoutInflater.from(this))
        dialoginding.txtService.text = getString(R.string.request_accepted)
        dialoginding.serviceStatus.text = getString(R.string.thank_you_for_accepting)

        dialog.setContentView(dialoginding.root)
        dialog.setCancelable(false)
        dialog.show()

        dialog.window?.setBackgroundDrawableResource(android.R.color.transparent)
        val window: Window = dialog.window!!
        window.setLayout(
            ConstraintLayout.LayoutParams.MATCH_PARENT,
            ConstraintLayout.LayoutParams.MATCH_PARENT
        )

        dialoginding.btnOk.setCommonButtonListener {
//            ratingDialog()
            dialog.dismiss()

            Log.e("hello", "ratingDialogue::${dialoginding.btnOk}")
        }

        dialoginding.icCross.setOnClickListener {
            dialog.dismiss()
        }
    }


    fun ratingDialog() {
        val dialog = Dialog(this, R.style.RoundedCornersDialogRating)
        var dialoginding = DialogueFeedbackBinding.inflate(LayoutInflater.from(this))

        dialog.setContentView(dialoginding.root)
        dialog.setCancelable(false)
        dialog.show()

        dialog.window?.setBackgroundDrawableResource(android.R.color.transparent)
        val window: Window = dialog.window!!
        window.setLayout(
            ConstraintLayout.LayoutParams.MATCH_PARENT,
            ConstraintLayout.LayoutParams.WRAP_CONTENT
        )

        dialoginding.btnSubmit.setCommonButtonListener {
            dialog.dismiss()

        }

        dialoginding.icCross.setOnClickListener {
            dialog.dismiss()
        }
    }

    private fun openDialogStartService() {

        val dialog = Dialog(this, R.style.RoundedCornersDialogRating)
        var dialoginding = DialogServiceCompletedBinding.inflate(LayoutInflater.from(this))
        dialoginding.txtService.text = getString(R.string.service_started)
        dialoginding.serviceStatus.text = getString(R.string.service_is_in_progress)

        dialog.setContentView(dialoginding.root)
        dialog.setCancelable(false)
        dialog.show()

        dialog.window?.setBackgroundDrawableResource(android.R.color.transparent)
        val window: Window = dialog.window!!
        window.setLayout(
            ConstraintLayout.LayoutParams.MATCH_PARENT,
            ConstraintLayout.LayoutParams.MATCH_PARENT
        )

        dialoginding.btnOk.setCommonButtonListener {
            dialog.dismiss()
        }

        dialoginding.icCross.setOnClickListener {
            dialog.dismiss()
        }
    }

    private fun openDialogCompleted() {

        val dialog = Dialog(this, R.style.RoundedCornersDialogRating)
        var dialoginding = DialogServiceCompletedBinding.inflate(LayoutInflater.from(this))

        dialog.setContentView(dialoginding.root)
        dialog.setCancelable(false)
        dialog.show()

        dialog.window?.setBackgroundDrawableResource(android.R.color.transparent)
        val window: Window = dialog.window!!
        window.setLayout(
            ConstraintLayout.LayoutParams.MATCH_PARENT,
            ConstraintLayout.LayoutParams.MATCH_PARENT
        )

        dialoginding.btnOk.setCommonButtonListener {
            ratingDialog()
            dialog.dismiss()
        }

        dialoginding.icCross.setOnClickListener {
            dialog.dismiss()
        }
    }

    private fun initt() {

        if (intent != null) {
            position = intent.getIntExtra(IntentKey.POSITION, 0)
        }

        when (position) {
            0 -> { // My Request Accept Reject
                binding.rejectAccept.visible()
                binding.tvServiceQuantity.visible()
                binding.tvServiceQuantityNo.visible()
            }
            1 -> { // In Progress & Cancel
                binding.completeCancel.visible()
            }
            2 -> { // when service completed
                binding.imageCompleted.visible()
                binding.relRating.visible()
            }
            3 -> { // start service
                binding.layStartService.visible()
            }
        }
    }

    override fun onClickSendMessage() {
        if (SystemClock.elapsedRealtime() - Constants.mLastClickTime < 1000) {
            return
        }
        Constants.mLastClickTime = SystemClock.elapsedRealtime()
        startNewActivity(ChatActivity::class.java)
    }

    override fun onMapReady(p0: GoogleMap) {

        mMap = p0

        val latLong = LatLng(23.033863, 72.585022)
        val marker = MarkerOptions().position(latLong)

        val bitmap = ContextCompat.getDrawable(this, R.drawable.ic_marker)?.toBitmap(50, 70)
        marker.icon(bitmap?.let { BitmapDescriptorFactory.fromBitmap(it) })

        mMap?.addMarker(marker)
        mMap?.moveCamera(CameraUpdateFactory.newLatLng(latLong))
        mMap?.animateCamera(CameraUpdateFactory.zoomTo(15f))


        mMap = p0
        mMap!!.mapType = GoogleMap.MAP_TYPE_NORMAL
        mMap!!.uiSettings.isZoomControlsEnabled = true

        val mapFragment =
            supportFragmentManager.findFragmentById(R.id.mapView) as MapFragment?

        mapFragment?.setListener(object :
            MapFragment.OnTouchListener {
            override fun onTouch() {
                binding.scrollView.requestDisallowInterceptTouchEvent(true)
            }
        })

    }

    override fun setupObservable() {

    }

}