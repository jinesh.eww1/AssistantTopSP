package com.assistant.top.service.provider.util.network

interface API_CONSTANTS {

    companion object {
        var BASE_URL: String = "http://assistentetop.excellentwebworld.in/api/"
        var HEADER_KEY = "key"
        var HEADER_VALUE = "AssistentETop#1"
        var X_API_KEY = "x-api-key"

        var ENDPOINT_INIT = BASE_URL.plus("Common_api/init/android_customer/")
        var ENDPOINT_LOGOUT = BASE_URL.plus("Service_Provider_api/logout/")


        const val WEB_SERVICE_LOGIN = "Service_Provider_api/login"
        const val WEB_SERVICE_REGISTER = "Service_Provider_api/register"
        const val WEB_SERVICE_FORGOT = "Service_Provider_api/forgot_password"
        const val WEB_SERVICE_PROFILE_UPDATE = "Service_Provider_api/profile_update"
        const val WEB_SERVICE_CHANGE_PWD = "Service_Provider_api/password_update"
        const val WEB_SERVICE_HOME_DATA = "Service_Provider_api/home"



        /* Add services */
        const val WEB_SERVICE_ADD_SERVICES = "Service_Provider_api/get_all_services"

        /* choose plan */
        const val WEB_SERVICE_CHOOSE_PLAN = "Service_Provider_api/get_all_plans"


        /*language label*/
        const val WEB_SERVICE_LANGUAGE_LABEL = "Common_api/language_labels"
        const val WEB_PARAM_LANGUAGE_LABEL_CODE = "code"

        /* login screen */
        const val WEB_PARAM_EMAIL_OR_PHONE = "email_or_phone"
        const val WEB_PARAM_DEVICE_TYPE = "device_type"
        const val WEB_PARAM_DEVICE_TOKEN = "device_token"

        /* sign up screen */

        const val WEB_PARAM_FIRST_NAME = "first_name"
        const val WEB_PARAM_LAST_NAME = "last_name"
        const val WEB_PARAM_EMAIL = "email"
        const val WEB_PARAM_MOBILE_NUMBER = "mobile_number"
        const val WEB_PARAM_ADDRESS = "address"
        const val WEB_PARAM_GENDER = "gender"
        const val WEB_PARAM_PASSWORD = "password"
        const val WEB_PARAM_SERVICE_ID = "service_ids"
        const val WEB_PARAM_PLAN_ID = "plan_id"
        const val WEB_PARAM_PROFILE_IMAGE = "profile_image"

        /* profile data */

        const val WEB_PARAM_USER_ID = "user_id"
        const val WEB_PARAM_USER_ROLE = "role"
        const val WEB_PARAM_NIF_NUMBER = "nif_number"
        const val WEB_PARAM_ABOUT_US = "about_us"


        /* change password */

        const val WEB_PARAM_CURRENT_PWD = "current_password"
        const val WEB_PARAM_NEW_PWD = "new_password"
        const val WEB_PARAM_CONFORM_PWD = "confirm_password"



    }
}