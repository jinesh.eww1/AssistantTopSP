package com.assistant.top.service.provider.extension

import android.annotation.SuppressLint
import android.content.Context
import android.util.Log

const val TAG = "AssistantTopServiceProvider"
@SuppressLint("LongLogTag")
fun Context.logMessage(tag: String = "AssistantTopServiceProvider", msg: String) {
    Log.e(TAG + tag, "= $msg")
}
