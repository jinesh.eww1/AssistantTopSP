package com.assistant.top.service.provider.ui.addServices

import android.content.Context
import android.util.Log
import android.view.View
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.assistant.top.service.provider.BuildConfig
import com.assistant.top.service.provider.base.BaseViewModel
import com.assistant.top.service.provider.data.Response.AddServicesResponse
import com.assistant.top.service.provider.data.Response.InitResponse
import com.assistant.top.service.provider.data.Response.LoginResponse
import com.assistant.top.service.provider.prefs.UserPreference
import com.assistant.top.service.provider.util.AppLog
import com.assistant.top.service.provider.util.network.API_CONSTANTS
import com.assistant.top.service.provider.util.network.NetworkService
import com.assistant.top.service.provider.util.network.NetworkUtils
import com.assistant.top.service.provider.util.network.Resource
import com.assistant.top.service.provider.utility.Constants
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.Response
import javax.inject.Inject

class AddServiceViewModel @Inject constructor(
    var context: Context,
    var userPreference: UserPreference,
    var networkService: NetworkService
) : BaseViewModel<AddServicesNavigator>() {

    private val addServiceResponseObservable: MutableLiveData<Resource<AddServicesResponse>> =
        MutableLiveData()

    fun getAddServiceObservable(): LiveData<Resource<AddServicesResponse>> {
        return addServiceResponseObservable
    }

    fun addServices(){
        if (NetworkUtils.isNetworkConnected(context)) {
            var response: Response<AddServicesResponse>? = null
            viewModelScope.launch {
                addServiceResponseObservable.value = Resource.loading(null)
                withContext(Dispatchers.IO) {
                    AppLog.e(Constants.TAG, "$response")
                    response = networkService.addServices(API_CONSTANTS.WEB_SERVICE_ADD_SERVICES)
                }
                withContext(Dispatchers.Main) {
                    response.run {
                        AppLog.e(Constants.TAG, "${baseDataSource.getResult(true) { this!! }}")
                        addServiceResponseObservable.value = baseDataSource.getResult(true) { this!! }
                    }
                }
            }
        } else {
            viewModelScope.launch {
                withContext(Dispatchers.Main) {
                    addServiceResponseObservable.value = Resource.noInternetConnection(null)
                }
            }
        }
    }

}