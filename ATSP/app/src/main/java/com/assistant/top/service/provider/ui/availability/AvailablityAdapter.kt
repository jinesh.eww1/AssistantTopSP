package com.assistant.top.service.provider.ui.serviceDetail

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.assistant.top.service.provider.R
import com.assistant.top.service.provider.databinding.AvailabilityListBinding
import com.assistant.top.service.provider.ui.availability.AvailabilityActivity

class AvailablityAdapter(
   val availabilityActivity: AvailabilityActivity,
   var availableDetailListing: ArrayList<com.assistant.top.service.provider.ui.availability.AvailabilityViewModel>
) : RecyclerView.Adapter<AvailablityAdapter.ServiceDetailViewHolder>() {

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int,
    ): AvailablityAdapter.ServiceDetailViewHolder =
        ServiceDetailViewHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.availability_list,
                parent,
                false
            )
        )

    override fun onBindViewHolder(
        holder: ServiceDetailViewHolder,
        position: Int,
    ) {
    }


    override fun getItemCount(): Int {
        return 2
    }


    inner class ServiceDetailViewHolder(
        val binding: AvailabilityListBinding,
    ) : RecyclerView.ViewHolder(binding.root)

}
