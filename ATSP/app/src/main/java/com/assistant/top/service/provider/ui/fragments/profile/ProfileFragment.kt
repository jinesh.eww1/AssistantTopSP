package com.assistant.top.service.provider.ui.fragments.profile

import android.annotation.SuppressLint
import android.app.Activity
import android.app.AlertDialog
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.*
import android.provider.MediaStore
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.core.content.FileProvider
import com.assistant.top.service.provider.BR
import com.assistant.top.service.provider.R
import com.assistant.top.service.provider.base.BaseFragment
import com.assistant.top.service.provider.databinding.FragmentProfileBinding
import com.assistant.top.service.provider.extension.*
import com.assistant.top.service.provider.ui.changepassword.ChangePasswordActivity
import com.assistant.top.service.provider.ui.edit_service.EditServiceActivity
import com.assistant.top.service.provider.ui.home.HomeActivity
import com.assistant.top.service.provider.util.AppLog
import com.assistant.top.service.provider.util.Validation
import com.assistant.top.service.provider.util.network.AppConstants
import com.assistant.top.service.provider.util.network.Resource
import com.assistant.top.service.provider.utility.Constants
import com.assistentetop.customer.utils.alertDialog.*
import dagger.hilt.android.AndroidEntryPoint
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


@AndroidEntryPoint
class ProfileFragment : BaseFragment<FragmentProfileBinding, ProfileViewModel>(), ProfileNavigator {

    var timerr: CountDownTimer? = null
    var TAG = "ProfileFragment"
    lateinit var onProfileNavigator: ProfileNavigator
    override val layoutId: Int get() = R.layout.fragment_profile
    override val bindingVariable: Int get() = BR.viewmodel

    val REQUEST_ID_MULTIPLE_PERMISSIONS = 101
    var photoFile: File? = null
    var userSelect = false

    var REQUEST_GALLARY = 1
    var REQUEST_CAMERA = 2
    var userProfileImage: Uri? = null

    private var mCameraImageFilePath: String? = null

    private var imageOptionSelect = -1
    private val PERMISSION_CAMERA = 1000
    private val PERMISSION_GALLERY = 2000

    @SuppressLint("ClickableViewAccessibility")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initt()
        setOnEventListener(this)
        setGenderSpinner()
        setMyServiceAdapter()
        profileDataDisplay()

        mViewModel.setLanguageObject(language!!)
        binding.userProfileIcon.isEnabled = false
        binding.userProfileIcon.isClickable = false

        binding.txtEditBtn.setOnClickListener {
            startActivity(Intent(context,EditServiceActivity::class.java))
        }

        binding.edtAboutUs.setOnTouchListener { v, _ ->
            binding.scrollView.requestDisallowInterceptTouchEvent(true)
            false
        }

        binding.userProfileIcon.setOnClickListener {
            isChangeProfile()

        }

        binding.btnSave.setCommonButtonListener {
            binding.btnSave.isEnabled = false
            mViewModel.updateProfile()
        }
    }


//    private fun focusOnView() {
//        binding.scrollView.fullScroll(View.FOCUS_UP)
//    }

    private fun profileDataDisplay() {

        Log.e(TAG, "valueee ${userPreference.getUserGender()}")
        if (userPreference.getUserGender().equals(getString(R.string.female))) {
            binding.spinner.setSelection(1)
        } else {
            binding.spinner.setSelection(0)
        }


        mViewModel.apply {
            firstName = userPreference.getFirstName()
            lastName = userPreference.getLastName()
            emailAddress = userPreference.getEmailAddress()
            phoneNumber = userPreference.getPhoneNo()
            gender = userPreference.getUserGender()
            about_us = userPreference.getUserAboutUs()
            address = userPreference.getUserAddress()
            binding.viewModel = mViewModel
        }
        binding.userProfileIcon.loadImage(userPreference.getUserImage())

        Log.e(TAG, "userImage:=${userPreference.getUserImage()}")
        Log.e(TAG, "getFirstName:=${userPreference.getFirstName()}")
        Log.e(TAG, "getLastName:=${userPreference.getLastName()}")
        Log.e(TAG, "getEmailAddress:=${userPreference.getEmailAddress()}")
        Log.e(TAG, "getPhoneNo:=${userPreference.getPhoneNo()}")
        Log.e(TAG, "getUserGender:=${userPreference.getUserGender()}")
    }


    private fun initt() {

        mViewModel.setNavigator(this)
    }

    private fun setMyServiceAdapter() {

        binding.rvMyService.adapter = MyServiceAdapter()

    }

    private fun setGenderSpinner() {

        val genderList = ArrayList<String>()
        genderList.add(getString(R.string.male))
        genderList.add(getString(R.string.female))
        val itemsAdapter =
            ArrayAdapter(requireContext(), R.layout.spinner_item, genderList)

        itemsAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        binding.spinner.adapter = itemsAdapter
        binding.spinner.setEnabled(false)
        binding.spinner.setClickable(false)
        binding.spinner.adapter = itemsAdapter
        binding.spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parentView: AdapterView<*>?,
                selectedItemView: View?,
                position: Int,
                id: Long
            ) {
                val text: String = parentView?.getItemAtPosition(position).toString()
                mViewModel.gender = text
                itemsAdapter.notifyDataSetChanged()
                Log.e(TAG, "gendervalue:=>$text")
            }

            override fun onNothingSelected(parentView: AdapterView<*>?) {
                // your code for nothing
            }
        }
    }

    override fun onClickEditButton() {

        binding.btnSave.visible()
        binding.edtFirstName.enabled()
        binding.edtLastName.enabled()
        binding.edtEmailAddress.disabled()
        binding.edtPhoneNumber.enabled()
        binding.edtAddress.enabled()
        binding.edtNIF.enabled()
        binding.edtAboutUs.enabled()
        binding.spinner.enabled()
        binding.txtSpinner.enabled()
        binding.userProfileIcon.isEnabled = true
        binding.userProfileIcon.isClickable = true
        binding.spinner.isEnabled = true
        binding.spinner.isClickable = true

    }

    override fun onClickSaveButton() {


        binding.btnSave.gone()
        binding.edtFirstName.disabled()
        binding.edtLastName.disabled()
        binding.edtEmailAddress.disabled()
        binding.edtPhoneNumber.disabled()
        binding.edtAddress.disabled()
        binding.edtNIF.disabled()
        binding.edtAboutUs.disabled()
        binding.spinner.isEnabled = false
        binding.spinner.isClickable = false
        binding.txtSpinner.setEnabled(false)
        binding.txtSpinner.setClickable(false)
        binding.userProfileIcon.setEnabled(false)
        binding.userProfileIcon.setClickable(false)

        (activity as HomeActivity).binding.commonToolbar.binding.ivNotification.visible()

    }


    override fun onClickChangePassword() {

        if (SystemClock.elapsedRealtime() - Constants.mLastClickTime < 1000) {
            return
        }

        Constants.mLastClickTime = SystemClock.elapsedRealtime()
        requireActivity().startNewActivity(ChangePasswordActivity::class.java)
    }

    override fun showLoader() {
        binding.btnSave.setLoading(true, userPreference)

    }

    /*override fun onClickSpinner() {

        binding.txtSpinner.gone()
        binding.spinner.visible()
        binding.spinner.performClick()
    }*/

    fun isChangeProfile() {
        val options =
            arrayOf<CharSequence>(
                getString(R.string.take_photo),
                getString(R.string.Choose_from_Gallery)
            )

        val builder = AlertDialog.Builder(requireActivity())
        builder.setTitle(getString(R.string.add_photo))
        builder.setItems(options) { dialog, item ->
            if (item == 0) {
                imageOptionSelect = 0

                if (requireActivity().checkImagePermission()) {
                    this.requestImagePermissionFragment(PERMISSION_CAMERA)
                } else {
                    callCameraOpen()
                }
            } else if (item == 1) {
                imageOptionSelect = 1

                try {
                    if (requireActivity().checkImageGalleryPermission()) {

                        this.requestImageGalleryPermissionFragment(
                            PERMISSION_GALLERY
                        )
                    } else {
                        callGalleryOpen()
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }

            }

        }
        builder.show()
    }


    override fun onDestroy() {
        timerr?.cancel()
        super.onDestroy()
        Log.e("destroy", "destroy:=>$timerr")
    }

    override fun getExitTransition(): Any? {
        timerr?.cancel()
        return super.getExitTransition()
    }

    fun callCameraOpen() {
        Handler(Looper.getMainLooper()).postDelayed(
            { takePictureFromCamera() },
            300
        )
    }

    fun callGalleryOpen() {
        val intent =
            Intent(
                Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI
            )
        startActivityForResult(intent, REQUEST_GALLARY)
    }

    private fun takePictureFromCamera() {
        val pictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        if (pictureIntent.resolveActivity(requireActivity().getPackageManager()) != null) {
            var cameraFile: File? = null
            try {
                cameraFile = createImageFile()
            } catch (ex: IOException) {
                ex.printStackTrace()
            }
            if (cameraFile != null) {
                val photoURI = FileProvider
                    .getUriForFile(
                        requireActivity(),
                        getString(R.string.pakageName_provider),
                        cameraFile
                    )

                pictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
                startActivityForResult(pictureIntent, REQUEST_CAMERA)
            }
        }

    }

    @Throws(IOException::class)
    private fun createImageFile(): File? {
        val timeStamp: String = SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault())
            .format(Date())
        val imageFileName = "IMG_" + timeStamp + "_"
        val storageDir: File? =
            requireActivity().getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        val ImageFile = File.createTempFile(
            imageFileName,
            ".jpg",
            storageDir
        )
        mCameraImageFilePath = "file:" + ImageFile.absolutePath
        return ImageFile
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        AppLog.e(TAG, "camera permission")
        if (resultCode == Activity.RESULT_OK) {

            if (requestCode == REQUEST_CAMERA) {

                userProfileImage = Uri.parse(mCameraImageFilePath)
                binding.userProfileIcon.setImageURI(userProfileImage).toString()
                mViewModel.imageUri = userProfileImage
                AppLog.e(TAG, "cameraPermission:=>$userProfileImage")


            } else if (requestCode == REQUEST_GALLARY) {

                val selectedImage: Uri? = data?.data
                binding.userProfileIcon.setImageURI(selectedImage)
                mViewModel.imageUri = selectedImage

            }
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String?>,
        grantResults: IntArray
    ) {
       AppLog.e(TAG, "onRequestPermissionsResult here...")
        if (requestCode == PERMISSION_CAMERA || requestCode == PERMISSION_GALLERY) {
            var shouldRationale = true
            for (permission in permissions) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (shouldShowRequestPermissionRationale(permission!!)) {
                        shouldRationale = false
                        break
                    }
                }
            }
            if (grantResults.size > 0) {
                if (permissions.size == 2 && grantResults.size == 2 && (grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED)) {
                    Log.e("permissionGranted", "granted...")
                    if (imageOptionSelect == 0) {
                        callCameraOpen()
                    } else if (imageOptionSelect == 1) {
                        callGalleryOpen()
                    }
                } else if (permissions.size == 1 && grantResults.size == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.e("permissionGranted", "granted...")
                    if (imageOptionSelect == 0) {
                        callCameraOpen()
                    } else if (imageOptionSelect == 1) {
                        callGalleryOpen()
                    }
                } else if (Build.VERSION.SDK_INT >= 23 && shouldRationale) {
                    requireActivity().showLocationPermissionNeeded()
                } else {
                    requireActivity().showLocationPermissionNeeded()
                }
            } else if (Build.VERSION.SDK_INT >= 23 && shouldRationale) {
                requireActivity().showLocationPermissionNeeded()
            } else {
                requireActivity().showLocationPermissionNeeded()
            }
        }
    }

    fun setOnEventListener(listener: ProfileNavigator) {
        onProfileNavigator = listener
    }

    override fun setupObservable() {
        mViewModel.getValidationStatus().observe(this, {
            Validation.showMessageDialog(requireActivity(), it)
        })

        mViewModel.getProfileDataObservable().observe(this, {
            when (it.status) {
                Resource.Status.SUCCESS -> {
                    onClickSaveButton()
                    binding.btnSave.setLoading(false, userPreference)
                    it.let {
                        val data = gson.toJson(it.data)
                        userPreference.saveUserSession(AppConstants.SharedPrefKey.USER_PREF, data)
                        showSuccessAlert(language?.getLanguage(it.message)?:"")

                        Log.e("saveUserSession", "saveUserSession::${data}")
                    }
                }

                Resource.Status.ERROR -> {
                    binding.btnSave.setLoading(false, userPreference)

                    if (!checkIsSessionOut(it.code)) {
                        it.message?.let { message ->
                            requireActivity().alertDialog(message = if (com.assistentetop.customer.utils.alertDialog.checkIsConnectionReset(
                                    it.code
                                )
                            ) {
                                getString(R.string.connection_reset)
                            } else {
                                message
                            })

                        }
                        Log.e(TAG, "On Error ${it.message} ")
                    }

                }

                Resource.Status.LOADING -> {
                    //   showLoaderDialog()
                    //viewDataBinding.btnSave.setLoading(true, userPreference)
                    Log.e("TAG", "On LOADING ${it.message} ")

                }

                Resource.Status.NO_INTERNET_CONNECTION -> {
                    Log.e("TAG", "On NO_INTERNET_CONNECTION ${it.message} ")

                }
            }
        })
    }

}