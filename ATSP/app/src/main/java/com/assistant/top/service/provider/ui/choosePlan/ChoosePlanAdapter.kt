package com.assistant.top.service.provider.ui.choosePlan

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.os.SystemClock
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.assistant.top.service.provider.R
import com.assistant.top.service.provider.data.Response.ChoosePlanResponse
import com.assistant.top.service.provider.databinding.ChoosePlanItemBoxBinding
import com.assistant.top.service.provider.extension.*
import com.assistant.top.service.provider.language.KEY
import com.assistant.top.service.provider.language.Language
import com.assistant.top.service.provider.util.AppLog
import com.assistant.top.service.provider.utility.Constants


class ChoosePlanAdapter(
    var navigator: ChoosePlanNavigator?,
    var choosePlanList: ArrayList<ChoosePlanResponse.plansData>,
    var language: Language?,
) :
    RecyclerView.Adapter<ChoosePlanAdapter.MenuHolder>() {

    private var selectedPosition = 0
    lateinit var context: Context
    lateinit var activity: Activity
    lateinit var binding: ChoosePlanItemBoxBinding

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MenuHolder {

        context = parent.context
        activity = Activity()
        binding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.choose_plan_item_box,
            parent,
            false
        )

        return MenuHolder(binding)
    }

    override fun onBindViewHolder(holder: MenuHolder, @SuppressLint("RecyclerView") position: Int) {

        if (selectedPosition == position) {
            context.logMessage("ChoosePlanAdapter", "position = $position")
            holder.binding.boxItem.setRectDrawable(
                15f,
                ContextCompat.getColor(
                    context,
                    R.color.theme
                )
            )

            updateTextColor(holder, true)

        } else {
            holder.binding.boxItem.setBorderRectDrawable(
                15f,
                2,
                ContextCompat.getColor(
                    context,
                    R.color.theme
                ),
                ContextCompat.getColor(
                    context,
                    R.color.white
                ),
                ContextCompat.getColor(
                    context,
                    R.color.white
                )
            )
            updateTextColor(holder, false)
        }

        val planList = choosePlanList[position]
//        setData(holder, planList.name, planList.days)
        AppLog.e(TAG, "getLanguage => ${planList.name}")
        AppLog.e(TAG, "getLanguageee => ${language?.getLanguage(KEY.free)}")

        if (planList.name == language?.getLanguage(KEY.free)) {

            setData(holder, planList.name, planList.days.plus(language?.getLanguage(KEY.Days)))

        } else if (planList.name != language?.getLanguage(KEY.free)) {
            setData(holder, planList.price, planList.name)
        }

        AppLog.e(TAG, "id => ${planList.id}")
        AppLog.e(TAG, "name => ${planList.name}")
        AppLog.e(TAG, "price => ${planList.price}")
        AppLog.e(TAG, "days => ${planList.days}")

        holder.itemView.setOnClickListener {
            if (SystemClock.elapsedRealtime() - Constants.mLastClickTime < 1000) {
                return@setOnClickListener
            }
            Constants.mLastClickTime = SystemClock.elapsedRealtime()
            selectedPosition = position
            notifyDataSetChanged()
            navigator?.onItemClick()

        }
    }

    private fun updateTextColor(holder: MenuHolder, isSelectd: Boolean) {
        if (isSelectd) {
            holder.binding.txtAmount.setTextColor(ContextCompat.getColor(context, R.color.white))
            holder.binding.txtTimiLine.setTextColor(ContextCompat.getColor(context, R.color.white))
        } else {

            holder.binding.txtAmount.setTextColor(ContextCompat.getColor(context, R.color.theme))
            holder.binding.txtTimiLine.setTextColor(ContextCompat.getColor(context, R.color.theme))
        }
    }

    private fun setData(holder: MenuHolder, amount: String, timeLine: String) {

        holder.binding.txtAmount.text = amount
        holder.binding.txtTimiLine.text = timeLine

        AppLog.e(TAG, "names=> ${amount}")
        AppLog.e(TAG, "days=> ${timeLine}")
    }

    override fun getItemCount(): Int {
        //return 4
        return choosePlanList.size
    }

    class MenuHolder(var binding: ChoosePlanItemBoxBinding) : RecyclerView.ViewHolder(binding.root)
}