package com.assistant.top.service.provider.ui.home

import android.annotation.SuppressLint
import android.content.Context
import android.os.SystemClock
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.assistant.top.service.provider.R
import com.assistant.top.service.provider.databinding.DrawerItemBinding
import com.assistant.top.service.provider.extension.logMessage
import com.assistant.top.service.provider.utility.Constants

class MenuAdapter(
    var menuList: ArrayList<MenuItemModel>,
    var clickListener: HomeActivityNavigator
) : RecyclerView.Adapter<MenuAdapter.MenuHolder>() {

    lateinit var context: Context
    lateinit var binding: DrawerItemBinding

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MenuHolder {

        context = parent.context
        binding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.drawer_item,
            parent,
            false
        )

        return MenuHolder(binding)
    }

    @SuppressLint("NotifyDataSetChanged")
    override fun onBindViewHolder(holder: MenuHolder, @SuppressLint("RecyclerView") position: Int) {
        holder.binding.itemName.text = menuList[position].itemName
        holder.binding.itemImage.setImageResource(menuList[position].image)

        context.logMessage("MenuAdapter", Constants.CURRENT_POSITION .toString())

        if (position == Constants.CURRENT_POSITION) {
            selectCurrentItem(holder)
        } else {
            removeCurrentItem(holder)
        }

        holder.itemView.setOnClickListener {
            if (SystemClock.elapsedRealtime() - Constants.mLastClickTime < 1000){
                return@setOnClickListener
            }
            Constants.mLastClickTime = SystemClock.elapsedRealtime()

            Constants.CURRENT_POSITION = position
            clickListener.onClickDrawerItem(position)
            notifyDataSetChanged()
        }
    }

    private fun removeCurrentItem(holder: MenuHolder) {
        holder.binding.itemImage.setColorFilter(ContextCompat.getColor(context, R.color.grey))
        holder.binding.itemName.setTextColor(ContextCompat.getColor(context, R.color.grey))
        holder.binding.layParent.setBackgroundColor(
            ContextCompat.getColor(
                context,
                android.R.color.transparent
            )
        )
    }

    private fun selectCurrentItem(holder: MenuHolder) {
        holder.binding.itemImage.setColorFilter(ContextCompat.getColor(context, R.color.yellow))
        holder.binding.itemName.setTextColor(ContextCompat.getColor(context, R.color.yellow))
        holder.binding.layParent.setBackgroundColor(
            ContextCompat.getColor(
                context,
                R.color.selected_menu_item_color
            )
        )
    }

    override fun getItemCount(): Int {
        return menuList.size
    }

    class MenuHolder(var binding: DrawerItemBinding) : RecyclerView.ViewHolder(binding.root)
}