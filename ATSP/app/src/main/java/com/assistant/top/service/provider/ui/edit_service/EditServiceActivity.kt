package com.assistant.top.service.provider.ui.edit_service

import android.os.Bundle
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.viewpager.widget.ViewPager
import com.assistant.top.service.provider.BR
import com.assistant.top.service.provider.R
import com.assistant.top.service.provider.base.BaseActivity
import com.assistant.top.service.provider.databinding.ActivityEditBinding
import com.assistant.top.service.provider.extension.setRectDrawable
import com.assistant.top.service.provider.ui.myRequestWithFragment.MyPagerAdapter
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class EditServiceActivity : BaseActivity<ActivityEditBinding, EditServiceModel>(), EditServiceNavigator {

    private lateinit var adapterViewPager: MyServicePagerAdapter
    override val layoutId: Int get() = R.layout.activity_edit
    override val bindingVariable: Int get() = BR.viewmodel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mViewModel.setNavigator(this)

        setViewPager()
    }

    private fun setViewPager() {

        adapterViewPager = MyServicePagerAdapter(supportFragmentManager)
        binding.mViewPager.adapter = adapterViewPager

        binding.mViewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {

            }

            override fun onPageSelected(position: Int) {
                onTabChange(position)
            }

            override fun onPageScrollStateChanged(state: Int) {

            }

        })

    }

    override fun onClickGenralService() {
        updateTabBar(binding.txtGeneralTab, binding.txtAppointmentTab)

    }

    override fun onClickAppointmentService() {
        updateTabBar(binding.txtAppointmentTab, binding.txtGeneralTab)
    }

    override fun onTabChange(position: Int) {

    }

    private fun updateTabBar(firstView: TextView, secondView: TextView) {
        secondView.setTextColor(ContextCompat.getColor(this, R.color.my_request_color))
        secondView.setBackgroundColor(ContextCompat.getColor(this, R.color.white))
        firstView.setTextColor(ContextCompat.getColor(this, R.color.white))
        firstView.setRectDrawable(90f, ContextCompat.getColor(this, R.color.theme))
    }

    override fun setupObservable() {

    }
}