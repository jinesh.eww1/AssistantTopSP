package com.assistant.top.service.provider.ui.fragments.myServices

import android.os.Bundle
import android.view.View
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import com.assistant.top.service.provider.BR
import com.assistant.top.service.provider.R
import com.assistant.top.service.provider.base.BaseFragment
import com.assistant.top.service.provider.databinding.FragmentMyServicesBinding
import com.assistant.top.service.provider.extension.startNewActivity
import com.assistant.top.service.provider.ui.choosePlan.ChoosePlanActivity
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MyServicesFragment : BaseFragment<FragmentMyServicesBinding, MyServiceViewModel>(),MyServiceNavigator {

    override val layoutId: Int get() = R.layout.fragment_my_services
    override val bindingVariable: Int get() = BR.viewmodel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mViewModel.setNavigator(this)

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setAdapter()
    }
    private fun setAdapter() {

        binding.rvAddRecyclerView.adapter = MyServicesAdapter()
    }

    override fun setupObservable() {

    }

}