package com.assistant.top.service.provider.ui.fragments.profile

interface ProfileNavigator {

    fun onClickEditButton()
    fun onClickSaveButton()
    fun onClickChangePassword()
    fun showLoader()
//    fun onClickSpinner()

}