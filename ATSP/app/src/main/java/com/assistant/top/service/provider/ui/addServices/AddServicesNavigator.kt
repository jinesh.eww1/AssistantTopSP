package com.assistant.top.service.provider.ui.addServices

interface AddServicesNavigator {

    fun onSignInNext()
    fun showLoader()

}