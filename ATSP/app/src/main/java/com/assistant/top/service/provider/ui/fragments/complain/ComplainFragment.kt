package com.assistant.top.service.provider.ui.fragments.complain

import android.os.Bundle
import android.view.View
import com.assistant.top.service.provider.BR
import com.assistant.top.service.provider.R
import com.assistant.top.service.provider.base.BaseFragment
import com.assistant.top.service.provider.databinding.FragmentHomeBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ComplainFragment : BaseFragment<FragmentHomeBinding,ComplainViewModel>() {

    override val layoutId: Int
        get() = R.layout.complain_fragment
    override val bindingVariable: Int
        get() = BR.viewModel

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mViewModel.setNavigator(this)
    }

    override fun setupObservable() {

    }


}