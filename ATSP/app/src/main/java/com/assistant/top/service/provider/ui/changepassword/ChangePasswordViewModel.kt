package com.assistant.top.service.provider.ui.changepassword

import android.content.Context
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.assistant.top.service.provider.base.BaseViewModel
import com.assistant.top.service.provider.data.Response.BaseResponse
import com.assistant.top.service.provider.prefs.UserPreference
import com.assistant.top.service.provider.util.ValidationStatus
import com.assistant.top.service.provider.util.isSpacePassword
import com.assistant.top.service.provider.util.isSpacePasswordContains
import com.assistant.top.service.provider.util.ispasswordValid
import com.assistant.top.service.provider.util.network.API_CONSTANTS
import com.assistant.top.service.provider.util.network.NetworkService
import com.assistant.top.service.provider.util.network.NetworkUtils
import com.assistant.top.service.provider.util.network.Resource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.Response
import javax.inject.Inject

class ChangePasswordViewModel @Inject constructor(
    val context: Context,
    private val networkService: NetworkService,
    private val userPreference: UserPreference
) : BaseViewModel<ChangePasswordNavigator>(){

    fun back(){
        getNavigator()?.gotoprofile()
    }

    var currentPwd: String = ""
    var newPwd: String = ""
    var conformPwd: String = ""

    private val changePwdObservable: MutableLiveData<Resource<BaseResponse>> =
        MutableLiveData()

    fun getChangePwdObservable(): LiveData<Resource<BaseResponse>> {
        return changePwdObservable
    }


    fun changePassword() {
        var response: Response<BaseResponse>? = null
        Log.e("response", "response =>  ${response}")

        if (NetworkUtils.isNetworkConnected(context)) {
            if (validationChangePwd()) {
                Log.e("validationChangePwd", "validationChangePwd =>  ${validationChangePwd()}")
                getNavigator()?.showLoader()

                viewModelScope.launch {
                    changePwdObservable.value = Resource.loading(null)
                    withContext(Dispatchers.IO) {
                        val changePwdRequest: MutableMap<String, String> = HashMap()

                        changePwdRequest[API_CONSTANTS.WEB_PARAM_USER_ID] = userPreference.getUserId()
                        changePwdRequest[API_CONSTANTS.WEB_PARAM_USER_ROLE] = userPreference.getUserRole()
                        changePwdRequest[API_CONSTANTS.WEB_PARAM_CURRENT_PWD] = currentPwd
                        changePwdRequest[API_CONSTANTS.WEB_PARAM_NEW_PWD] = newPwd
                        changePwdRequest[API_CONSTANTS.WEB_PARAM_CONFORM_PWD] = conformPwd

                        response = networkService.changePwd(changePwdRequest)

                    }

                    withContext(Dispatchers.Main) {
                        response.run {
                            changePwdObservable.value = baseDataSource.getResult { this!! }
                        }
                    }
                }
            }
        } else {
            viewModelScope.launch {
                withContext(Dispatchers.Main) {
                    changePwdObservable.value = Resource.noInternetConnection(null)
                }
            }
        }
    }

    fun validationChangePwd(): Boolean {
        when {
            currentPwd.isNullOrEmpty() -> {


                sendError(ValidationStatus.EMPTY_CURRENT_PASSWORD)
            }

            !currentPwd.isSpacePassword() -> {
                sendError(ValidationStatus.SPACE_PASSWORD_CURRENT)
            }

            !currentPwd.isSpacePasswordContains() -> {
                sendError(ValidationStatus.PASS_CONTAINS_MIDDLE_SPACE_CURRENT)
            }

            newPwd.isNullOrEmpty() -> {
                sendError(ValidationStatus.EMPTY_NEW_PASSWORD)
            }

            !newPwd.ispasswordValid() -> {
                sendError(ValidationStatus.CHANGE_PASS_LENGTH_NEW)
            }

            conformPwd.isNullOrEmpty() -> {
                sendError(ValidationStatus.EMPTY_CONFIRM_PASSWORD)
            }

            /* !conformPwd.ispasswordValid() -> {
                 sendError(ValidationStatus.PASSWORDVALIDATE)
             }*/

            !conformPwd.ispasswordValid() -> {
                sendError(ValidationStatus.CHANGE_PASS_LENGTH_CONF_PASS)
            }

            !newPwd.isSpacePassword() -> {
                sendError(ValidationStatus.SPACE_PASSWORD_NEW)
            }

            !newPwd.isSpacePasswordContains() -> {
                sendError(ValidationStatus.PASS_CONTAINS_MIDDLE_SPACE_NEW)
            }

            !(newPwd.equals(conformPwd)) -> {
                sendError(ValidationStatus.PASS_CONF_NOT_MATCH)
            }

            else -> {
                return true
            }
        }
        return false
    }

    private fun sendError(error: ValidationStatus) {
        getValidationStatus().postValue(error)
    }
}