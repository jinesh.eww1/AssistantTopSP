package com.assistant.top.service.provider.data.Response

import com.assistant.top.service.provider.data.Response.BaseResponse
import com.google.gson.annotations.SerializedName

class InitResponse(

    @SerializedName("update")
    val update: Boolean,

    @SerializedName("taxamount")
    val tax_amount: Int = 0,

    @SerializedName("tax_status")
    val tax_status: String,

    @SerializedName("tax_type")
    val tax_type: String,

    @SerializedName("maintenance")
    var maintenance: Boolean = false,

    ) : BaseResponse()