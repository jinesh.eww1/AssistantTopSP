package com.assistant.top.service.provider.ui.fragments.manageAvalibility

import android.annotation.SuppressLint
import android.content.Context
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.EditText
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.RecyclerView
import com.assistant.top.service.provider.R
import com.assistant.top.service.provider.databinding.SubWorkingDaysBinding
import com.assistant.top.service.provider.databinding.WorkingHoursItemBinding
import com.assistant.top.service.provider.extension.hideKeyboard
import com.assistant.top.service.provider.model.TimingList
import com.assistant.top.service.provider.model.WorkingHoursModel

class ManageAvailabilityAdapter(
    val requireActivity: FragmentActivity,
    val availableDetailListing: ArrayList<WorkingHoursModel>
) :
    RecyclerView.Adapter<ManageAvailabilityAdapter.MenuHolder>() {

    lateinit var context: Context
    lateinit var binding: WorkingHoursItemBinding

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MenuHolder {

        context = parent.context
        binding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.working_hours_item,
            parent,

            false
        )

        return MenuHolder(binding)
    }

    @SuppressLint("NotifyDataSetChanged")
    override fun onBindViewHolder(holder: MenuHolder, @SuppressLint("RecyclerView") position: Int) {

        holder.binding.txtDay.text = availableDetailListing[position].days
        holder.binding.rvTimeBlock.adapter = SubManageAvailabilityAdapter(
            availableDetailListing[position].timingList,
            requireActivity
        )


    }


    override fun getItemCount(): Int {
        return availableDetailListing.size
    }

    class MenuHolder(var binding: WorkingHoursItemBinding) :
        RecyclerView.ViewHolder(binding.root)

    class SubManageAvailabilityAdapter(
        var timingList: ArrayList<TimingList>,
        val requireActivity: FragmentActivity
    ) :
        RecyclerView.Adapter<SubManageAvailabilityAdapter.MenuHolder>() {

        lateinit var context: Context
        lateinit var binding: SubWorkingDaysBinding

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MenuHolder {

            context = parent.context
            binding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.sub_working_days,
                parent,

                false
            )

            return MenuHolder(binding)
        }

        @SuppressLint("NotifyDataSetChanged")
        override fun onBindViewHolder(
            holder: MenuHolder,
            @SuppressLint("RecyclerView") position: Int
        ) {

            if (position == 0) holder.binding.checkbox.text =
                requireActivity.getString(R.string.close) else
                holder.binding.checkbox.text = requireActivity.getString(R.string.Break)


            holder.binding.edtStartTime.setText(timingList[position].startTime)
            holder.binding.edtEndTime.setText(timingList[position].endTime)
            holder.binding.checkbox.isChecked = timingList[position].isChecked

            if (timingList[position].isChecked) {
                holder.binding.edtStartTime.isEnabled = false
                holder.binding.edtEndTime.isEnabled = false
            } else {
                holder.binding.edtStartTime.isEnabled = true
                holder.binding.edtEndTime.isEnabled = true
            }

            holder.binding.checkbox.setOnClickListener { v ->
                timingList[position].isChecked = (v as CheckBox).isChecked
                if (v.isChecked) {
                    requireActivity.hideKeyboard()
                }
                notifyDataSetChanged()
            }
        }


        override fun getItemCount(): Int {
            return timingList.size
        }

        class MenuHolder(var binding: SubWorkingDaysBinding) :
            RecyclerView.ViewHolder(binding.root)
    }

    class DateInputMask(val input: EditText) {

        fun listen() {
            input.addTextChangedListener(mDateEntryWatcher)
        }

        private val mDateEntryWatcher = object : TextWatcher {

            var edited = false
            val dividerCharacter = ":"

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                if (edited) {
                    edited = false
                    return
                }

                var working = getEditText()

                working = manageDateDivider(working, 2, start, before)
                working = manageDateDivider(working, 5, start, before)

                edited = true
                input.setText(working)
                input.setSelection(input.text.length)
            }

            private fun manageDateDivider(
                working: String,
                position: Int,
                start: Int,
                before: Int
            ): String {
                if (working.length == position) {
                    return if (before <= position && start < position)
                        working + dividerCharacter
                    else
                        working.dropLast(1)
                }
                return working
            }

            private fun getEditText(): String {
                return if (input.text.length >= 10)
                    input.text.toString().substring(0, 10)
                else
                    input.text.toString()
            }

            override fun afterTextChanged(s: Editable) {}
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
        }
    }
}