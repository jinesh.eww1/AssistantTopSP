package com.assistant.top.service.provider.util.network

import com.assistant.top.service.provider.data.Response.BaseResponse
import com.assistant.top.service.provider.util.AppLog
import retrofit2.Response
import java.lang.Exception

open class BaseDataSource {
    open suspend fun <T> getResult(
        isInit: Boolean = false,
        call: suspend () -> Response<T>,
    ): Resource<T> {
        try {
            val response = call()
            AppLog.e("responseCode", "isSuccessful => ${response.code()}")
            if (response.code() == 403) {
                return Resource.error("", code = response.code())
            } else if (response.code() == 502) {
                return Resource.error("", code = response.code())
            } else if (response.code() != 200) {
                return Resource.error(
                    "Something went wrong, please try after sometime",
                    code = response.code()
                )
            } else if (response.body() != null) {
                val baseResponse = (response.body() as BaseResponse)
                AppLog.e("responseCode", "isSuccessful => ${response.code()}")
                AppLog.e(
                    "responseCode",
                    "is If condition => ${(response.isSuccessful && baseResponse.status)}"
                )

                if (response.isSuccessful && baseResponse.status) {
                    response.body()?.let {
                        return Resource.success(it, baseResponse.message)
                    }
                } else {
                    if (isInit) {
                        response.body()?.let {
                            return Resource.success(it, baseResponse.message)
                        }
                    } else {
                        val body = response.body()
                        AppLog.e("response.body", "response.body => ${response.body()}")

                        if (body != null) {
                            AppLog.e("response.body", "response.bodyyyyy => ${response.body()}")
                            AppLog.e(
                                "response.body",
                                "response.bodyyyyy11111 => ${baseResponse.message}"
                            )

                            return Resource.error(baseResponse.message)
                        }
                    }
                }
            }
        } catch (e: Exception) {
            return Resource.error("Error => ${e.message} ?: ${e.toString()}")
        }
        return Resource.error("Internet Connection Issue")
    }

    private fun <T> error(message: String): Resource<T> {
        return Resource.error(message)
    }
}