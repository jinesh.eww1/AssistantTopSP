package com.assistant.top.service.provider.ui.chat

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.assistant.top.service.provider.R
import com.assistant.top.service.provider.databinding.ChatItemBinding
import com.assistant.top.service.provider.extension.gone
import com.assistant.top.service.provider.extension.visible

class ChatAdapter(val chatList: MutableList<ChatModel>, val chatActivity: ChatActivity) :
    RecyclerView.Adapter<ChatAdapter.MenuHolder>() {

    lateinit var context: Context
    lateinit var binding: ChatItemBinding

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MenuHolder {

        context = parent.context
        binding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.chat_item,
            parent,

            false
        )

        return MenuHolder(binding)
    }

    @SuppressLint("NotifyDataSetChanged")
    override fun onBindViewHolder(holder: MenuHolder, @SuppressLint("RecyclerView") position: Int) {

        val chat = chatList[position]

        if (chat.status == 0) {
            holder.binding.receiverView.visible()
            holder.binding.senderView.gone()

            binding.receiverView.text = chat.message

        } else {
            holder.binding.senderView.visible()
            holder.binding.receiverView.gone()

            binding.senderView.text = chat.message
        }

        if(chat.date!=""){
            binding.txtDate.visible()
        } else {
            binding.txtDate.gone()
        }

        binding.txtDate.text = chat.date

    }


    override fun getItemCount(): Int {
        return chatList.size
    }

    class MenuHolder(var binding: ChatItemBinding) :
        RecyclerView.ViewHolder(binding.root)
}