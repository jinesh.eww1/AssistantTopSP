package com.assistant.top.service.provider.data.Response

import com.google.gson.annotations.SerializedName

class HomeDataResponse(

    @SerializedName("graph_data")
    var graphDataList: ArrayList<graphData>,

    @SerializedName("balance")
    val balance: String,

    @SerializedName("service_requests")
    val service_requests: String,

    @SerializedName("pending_services")
    val pending_services: String,

    @SerializedName("in_progress_services")
    val in_progress_services: String,

    @SerializedName("completed_services")
    val completed_services: String,

    ) : BaseResponse() {

    data class graphData(
        @SerializedName("date")
        val date: String,

        @SerializedName("value")
        val value: String,

        )
}