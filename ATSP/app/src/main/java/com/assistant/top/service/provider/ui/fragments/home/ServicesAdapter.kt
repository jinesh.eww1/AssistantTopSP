package com.assistant.top.service.provider.ui.fragments.home

import android.annotation.SuppressLint
import android.content.Context
import android.os.SystemClock
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.assistant.top.service.provider.R
import com.assistant.top.service.provider.data.Response.HomeDataResponse
import com.assistant.top.service.provider.databinding.HomeServiceItemBoxBinding
import com.assistant.top.service.provider.utility.Constants

class ServicesAdapter(
    val navigator: HomeFragmentNavigator?,
    var homedataList: ArrayList<HomeDataResponse> = ArrayList(),
) : RecyclerView.Adapter<ServicesAdapter.MenuHolder>() {

    private var selectedPosition = 0
    lateinit var context: Context
    lateinit var binding: HomeServiceItemBoxBinding

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MenuHolder {

        context = parent.context
        binding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.home_service_item_box,
            parent,
            false
        )

        return MenuHolder(binding)
    }

    @SuppressLint("NotifyDataSetChanged", "ResourceAsColor")
    override fun onBindViewHolder(holder: MenuHolder, @SuppressLint("RecyclerView") position: Int) {

        if (selectedPosition == position) {
            holder.binding.boxItem.setBackgroundColor(
                ContextCompat.getColor(
                    context,
                    R.color.yellow
                )
            )
        } else {
            holder.binding.boxItem.setBackgroundColor(
                ContextCompat.getColor(
                    context,
                    R.color.box_bg
                )
            )
        }


        /*when (position) {
            0 -> {
                setData(holder, "02", "Services Requests")
            }
            1 -> {
                setData(holder, "10", "Pending Services")
            }
            2 -> {
                setData(holder, "02", "In Progress Services")
            }
            3 -> {
                setData(holder, "15", "Completed Services")
            }
        }*/

        val homeDataListing = homedataList[position]
        setData(holder,homeDataListing.service_requests,"")


        holder.itemView.setOnClickListener {
            if (SystemClock.elapsedRealtime() - Constants.mLastClickTime < 1000) {
                return@setOnClickListener
            }
            Constants.mLastClickTime = SystemClock.elapsedRealtime()
            selectedPosition = position
            notifyDataSetChanged()
            navigator?.onItemClick(position)

        }
    }

    private fun setData(holder: MenuHolder, count: String, serviceName: String) {

        holder.binding.serviceCount.text = count
        holder.binding.serviceName.text = serviceName
    }

    override fun getItemCount(): Int {
        return homedataList.size
    }

    class MenuHolder(var binding: HomeServiceItemBoxBinding) : RecyclerView.ViewHolder(binding.root)
}