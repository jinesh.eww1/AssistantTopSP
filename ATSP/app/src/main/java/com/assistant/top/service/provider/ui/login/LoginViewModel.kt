package com.assistant.top.service.provider.ui.login

import android.content.Context
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.assistant.top.service.provider.base.BaseViewModel
import com.assistant.top.service.provider.data.Response.LoginResponse
import com.assistant.top.service.provider.prefs.UserPreference
import com.assistant.top.service.provider.util.ValidationStatus
import com.assistant.top.service.provider.util.isEmailValid
import com.assistant.top.service.provider.util.isSpacePassword
import com.assistant.top.service.provider.util.ispasswordValid
import com.assistant.top.service.provider.util.network.API_CONSTANTS.Companion.WEB_PARAM_DEVICE_TOKEN
import com.assistant.top.service.provider.util.network.API_CONSTANTS.Companion.WEB_PARAM_DEVICE_TYPE
import com.assistant.top.service.provider.util.network.API_CONSTANTS.Companion.WEB_PARAM_EMAIL_OR_PHONE
import com.assistant.top.service.provider.util.network.API_CONSTANTS.Companion.WEB_PARAM_PASSWORD
import com.assistant.top.service.provider.util.network.NetworkService
import com.assistant.top.service.provider.util.network.NetworkUtils
import com.assistant.top.service.provider.util.network.Resource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.Response
import javax.inject.Inject

class LoginViewModel @Inject constructor(
    val context: Context,
    var networkService: NetworkService,
    var userPreference: UserPreference
) : BaseViewModel<LoginNavigator>(){

    var TAG = "LoginActivity"
    var emailAddress: String = ""
    var password: String = ""
    var fcm_device_token = "66676384312SDGSDGSDGSDGB"
    var socialId: String? = null
    var socialFirstName: String? = null
    var socialLastName: String? = null
    var socialUserName: String? = null
    var socialEmail: String? = null

    fun forgotPassword(){
        getNavigator()?.gotoForgotPassword()
    }

    fun signUp(){
        getNavigator()?.gotoSignUp()
    }


    private val loginResponseObservable: MutableLiveData<Resource<LoginResponse>> =
        MutableLiveData()

    fun getLoginObservable(): LiveData<Resource<LoginResponse>> {
        return loginResponseObservable
    }

    private val _validationState = MutableLiveData("")
    val validationStat = _validationState

    fun signin() {
        if (NetworkUtils.isNetworkConnected(context)) {
            Log.e(TAG, "loginReq =>")
            if (validationSignIn()) {
                lateinit var response: Response<LoginResponse>
                val loginRequest: MutableMap<String, String> = HashMap()
                loginRequest[WEB_PARAM_EMAIL_OR_PHONE] = emailAddress
                loginRequest[WEB_PARAM_PASSWORD] = password
                loginRequest[WEB_PARAM_DEVICE_TYPE] = "0"
                loginRequest[WEB_PARAM_DEVICE_TOKEN] = fcm_device_token

                Log.e(TAG, "loginReq => $loginRequest")
                getNavigator()?.showLoader()

                viewModelScope.launch {
                    loginResponseObservable.value = Resource.loading(null)
                    withContext(Dispatchers.IO) {
                        response = networkService.login(loginRequest)
                    }

                    withContext(Dispatchers.Main) {
                        response.run {
                            loginResponseObservable.value = baseDataSource.getResult { this }
                        }
                    }
                }

            }
        } else {
            viewModelScope.launch {
                withContext(Dispatchers.Main) {
                    loginResponseObservable.value = Resource.noInternetConnection(null)
                }
            }
        }
    }


    fun validationSignIn(): Boolean {

        when {
            emailAddress.isEmpty() -> {
                sendError(ValidationStatus.EMPTYEMAIL)
            }
            !emailAddress.isEmailValid() -> {
                sendError(ValidationStatus.InvalidEmail)
            }
            password.isEmpty() -> {
                sendError(ValidationStatus.PASSWORD)
            }
            !password.ispasswordValid() -> {
                sendError(ValidationStatus.PASSWORDVALIDATE)
            }
            !password.isSpacePassword() -> {
                sendError(ValidationStatus.SPACE_PASSWORD)
            }
            else -> {
                return true
            }
        }
        return false
    }

    private fun sendError(error: ValidationStatus) {
        getValidationStatus().postValue(error)
    }
}