package com.assistant.top.service.provider.prefs

import android.content.Context
import android.content.SharedPreferences
import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.assistant.top.service.provider.data.Response.LoginResponse
import com.assistant.top.service.provider.util.network.AppConstants
import com.google.gson.Gson
import javax.inject.Inject

open class UserPreference @Inject constructor(
    private val sharedPreferences: SharedPreferences,
    private val context: Context,
) {

    fun saveUserSession(key: String?, value: String?) {
        val editor = sharedPreferences.edit()
        editor.putString(key, value)
        editor.apply()
    }

    fun getUserSession(): LoginResponse? {
        val userSession = getUserSession(AppConstants.SharedPrefKey.USER_PREF, context)
        return if (userSession != null && !userSession.trim { it <= ' ' }
                .equals("", ignoreCase = true)) {
            Gson().fromJson(userSession, LoginResponse::class.java)
        } else null
    }

    fun getUserSessionKey(): String? {
        val userSession = getUserSession(AppConstants.SharedPrefKey.USER_PREF_KEY, context)
        return if (userSession != null && !userSession.trim { it <= ' ' }
                .equals("", ignoreCase = true)) {
            userSession
        } else ""
    }


    fun getUserRole(): String {
        if (isUserLogin()) {
            return getUserSession()!!.user_data.role
        }
        return ""
    }

    fun getUserAboutUs(): String {
        if (isUserLogin()) {
            return getUserSession()!!.user_data.about_us
        }
        return ""
    }

    fun getUserAddress(): String {
        if (isUserLogin()) {
            return getUserSession()!!.user_data.address
        }
        return ""
    }

    fun getUserId(): String {
        if (isUserLogin()) {
            return getUserSession()!!.user_data.id
        }
        return ""
    }

    fun getUserName(): String {
        if (isUserLogin()) {
            return getUserSession()!!.user_data.first_name.plus(" ")
                .plus(getUserSession()!!.user_data.last_name)
        }
        return ""
    }


    fun getFirstName(): String {
        if (isUserLogin()) {
            return getUserSession()!!.user_data.first_name
        }
        return ""
    }

    fun getLastName(): String {
        if (isUserLogin()) {
            return getUserSession()!!.user_data.last_name
        }
        return ""
    }

    fun getEmailAddress(): String {
        if (isUserLogin()) {
            return getUserSession()!!.user_data.email
        }
        return ""
    }

    fun getPhoneNo(): String {
        if (isUserLogin()) {
            return getUserSession()!!.user_data.mobile_number
        }
        return ""
    }

    fun getUserImage(): String {
        if (isUserLogin()) {
            return getUserSession()!!.user_data.profile_image
        } else {
            return ""
        }
    }

    fun getUserGender(): String {
        if (isUserLogin()) {
            return getUserSession()!!.user_data.gender
        } else {
            return ""
        }
    }

    fun getProgressButtonText(): String {
        return getUserSession()!!.progressButtonText
    }

    fun setButtonText(value : String) {
        getUserSession()!!.progressButtonText = value
    }

    fun getButtonText(): String {
        val userSession = getUserSession(AppConstants.SharedPrefKey.USER_PREF_BUTTON_TEXT, context)
        return if (userSession != null && !userSession.trim { it <= ' ' }
                .equals("", ignoreCase = true)) {
            userSession
        } else ""
    }


    @JvmName("getUName1")
    fun getUName(): MutableLiveData<String> {
        if (isUserLogin()) {
            return MutableLiveData(
                getUserSession()!!.user_data.first_name.plus(" ")
                    .plus(getUserSession()!!.user_data.last_name)
            )
        }
        return MutableLiveData("")
    }

   /* var uName: MutableLiveData<String> = if (isUserLogin()) {
        MutableLiveData<String>(
            getUserSession()!!.user_data.first_name.plus(" ")
                .plus(getUserSession()!!.user_data.last_name)
        )
    } else {
        MutableLiveData("")
    }*/

    fun getUserSession(key: String, context: Context?): String? {
        return if (context != null) {
            val prefs = sharedPreferences
            prefs.getString(key, "")
        } else {
            ""
        }
    }

    fun isUserLogin(): Boolean {
        return if (getUserSession() != null) {
            getUserSession()!!.status
        } else {
            false
        }
    }


    fun getAPIToken(): String {
        Log.e("getUserSession", "key -> ${getUserSessionKey()}")
        return getUserSessionKey()!!
        /*return if (getUserSession() != null) {
             getUserSession()!!.xApiKey
         } else {
             ""
         }
     return  ""*/
    }

    fun clearUserSession() {
        val editor = sharedPreferences.edit()
        editor.clear()
        editor.apply()
    }
}