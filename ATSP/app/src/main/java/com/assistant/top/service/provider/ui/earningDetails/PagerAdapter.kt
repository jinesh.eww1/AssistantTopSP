package com.assistant.top.service.provider.ui.earningDetails

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.assistant.top.service.provider.ui.earningDetails.all.AllFragment
import com.assistant.top.service.provider.ui.earningDetails.today.TodayAdapter
import com.assistant.top.service.provider.ui.earningDetails.today.TodayFragment
import com.assistant.top.service.provider.ui.earningDetails.week.WeekFragment
import com.assistant.top.service.provider.ui.earningDetails.yesterday.YesterdayFragment
import com.assistant.top.service.provider.ui.myRequestWithFragment.cancelled.CancelledFragment
import com.assistant.top.service.provider.ui.myRequestWithFragment.completed.CompletedFragment
import com.assistant.top.service.provider.ui.myRequestWithFragment.inProgress.InProgressFragment
import com.assistant.top.service.provider.ui.myRequestWithFragment.pending.PendingFragment


class PagerAdapter(fragmentManager: FragmentManager?) : FragmentPagerAdapter(fragmentManager!!) {
    override fun getCount(): Int {
        return NUM_ITEMS
    }

    // Returns the fragment to display for that page
    override fun getItem(position: Int): Fragment {
        return when (position) {
            0 -> AllFragment()
            1 -> TodayFragment()
            2 -> YesterdayFragment()
            3 -> WeekFragment()
            else -> AllFragment()
        }
    }

    // Returns the page title for the top indicator
    override fun getPageTitle(position: Int): CharSequence {
        return "Page $position"
    }

    companion object {
        private const val NUM_ITEMS = 4
    }
}