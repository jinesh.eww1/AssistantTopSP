package com.assistant.top.service.provider.ui.fragments.home

import android.content.Context
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.assistant.top.service.provider.base.BaseViewModel
import com.assistant.top.service.provider.data.Response.HomeDataResponse
import com.assistant.top.service.provider.data.Response.LoginResponse
import com.assistant.top.service.provider.extension.TAG
import com.assistant.top.service.provider.prefs.UserPreference
import com.assistant.top.service.provider.util.AppLog
import com.assistant.top.service.provider.util.network.API_CONSTANTS
import com.assistant.top.service.provider.util.network.NetworkService
import com.assistant.top.service.provider.util.network.NetworkUtils
import com.assistant.top.service.provider.util.network.Resource
import com.assistant.top.service.provider.utility.Constants
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.Response
import javax.inject.Inject

class HomeViewModel @Inject constructor(
    var context: Context,
    var userPreference: UserPreference,
    var networkService: NetworkService
) : BaseViewModel<HomeFragmentNavigator>() {

    fun onClickViewDetail(){
        Constants.logMessage("HomeViewModel","HomeViewModel")
        getNavigator()?.viewDetailClicked()
    }

    private val homeDataResponseObservable: MutableLiveData<Resource<HomeDataResponse>> =
        MutableLiveData()

    fun getHomeDataObservable(): LiveData<Resource<HomeDataResponse>> {
        return homeDataResponseObservable
    }

    fun homeData() {
        if (NetworkUtils.isNetworkConnected(context)) {
                lateinit var response: Response<HomeDataResponse>
                val homeDataRequest: MutableMap<String, String> = HashMap()

                homeDataRequest[API_CONSTANTS.WEB_PARAM_USER_ID] = userPreference.getUserId()
                homeDataRequest[API_CONSTANTS.WEB_PARAM_USER_ROLE] = userPreference.getUserRole()

                AppLog.e(TAG, "homedata => $homeDataRequest")
                //getNavigator()?.showLoader()

                viewModelScope.launch {
                    homeDataResponseObservable.value = Resource.loading(null)
                    withContext(Dispatchers.IO) {
                        response = networkService.homeData(homeDataRequest)
                    }

                    withContext(Dispatchers.Main) {
                        response.run {
                            homeDataResponseObservable.value = baseDataSource.getResult { this }
                        }
                    }
                }
        } else {
            viewModelScope.launch {
                withContext(Dispatchers.Main) {
                    homeDataResponseObservable.value = Resource.noInternetConnection(null)
                }
            }
        }
    }
}