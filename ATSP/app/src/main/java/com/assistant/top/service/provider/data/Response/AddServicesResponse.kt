package com.assistant.top.service.provider.data.Response

import android.net.Uri
import com.google.gson.annotations.SerializedName

class AddServicesResponse(

    @SerializedName("services")
    var serviceTypeList: ArrayList<ServiceTypeData>,

    ) : BaseResponse() {

    data class ServiceTypeData(
        @SerializedName("id")
        val id: String,

        @SerializedName("name")
        val name: String,

        @SerializedName("image")
        val image: String,

        @SerializedName("yellow_image")
        val yellow_image: String,


        )
}
