package com.assistant.top.service.provider.util.network

object AppConstants {

    var USER_PREFERENCE = "AssistantETop Service Provider"

    interface SharedPrefKey {
        companion object {
            const val USER_PREF = "user_pref"
            const val USER_PREF_KEY = "user_pref_key"
            const val USER_PREF_BUTTON_TEXT = "button_text"
            const val FCM_TOKEN = "fcm_token"
        }
    }
}