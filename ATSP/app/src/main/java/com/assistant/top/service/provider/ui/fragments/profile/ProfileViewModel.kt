package com.assistant.top.service.provider.ui.fragments.profile

import android.content.Context
import android.net.Uri
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import com.assistant.top.service.provider.base.BaseViewModel
import com.assistant.top.service.provider.data.Response.ProfileDataResponse
import com.assistant.top.service.provider.prefs.UserPreference
import com.assistant.top.service.provider.util.ValidationStatus
import com.assistant.top.service.provider.util.file.RealPathUtil
import com.assistant.top.service.provider.util.file.RealPathUtil.getRealPath
import com.assistant.top.service.provider.util.isEmailValid
import com.assistant.top.service.provider.util.isNameValid
import com.assistant.top.service.provider.util.isPhoneValid
import com.assistant.top.service.provider.util.network.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Response
import java.io.File
import javax.inject.Inject

class ProfileViewModel @Inject constructor(
    private val context: Context,
    private val networkService: NetworkService,
    private val userPreference: UserPreference
) : BaseViewModel<ProfileNavigator>() {

    fun onClickSave() {
        getNavigator()?.onClickSaveButton()
    }

    fun changePassword() {
        getNavigator()?.onClickChangePassword()
    }

    /*fun onClickSpinner() {
        getNavigator()?.onClickSpinner()
    }*/



    var TAG = "ProfileFragment"
    var firstName: String = ""
    var lastName: String = ""
    var userId: String = ""
    var emailAddress: String = ""
    var phoneNumber: String = ""
    var address: String = ""
    var gender = ""
    var about_us : String =  ""
    var nif_id = "123456789"
    var service_id = "2"
    var imageUri: Uri? = null


    private val profileDataObservable: SingleLiveEvent<Resource<ProfileDataResponse>> =
        SingleLiveEvent()

    fun getProfileDataObservable(): LiveData<Resource<ProfileDataResponse>> {
        return profileDataObservable
    }

    fun updateProfile() {
        if (NetworkUtils.isNetworkConnected(context)) {
            if (validate()) {


                var image: MultipartBody.Part? = null


                if (imageUri != null) {
                    val documentImage = File(getRealPath(context, imageUri!!))
                    val requestImageFile = RequestBody.create(
                        "multipart/form-data".toMediaTypeOrNull(),
                        documentImage
                    )

                    image = MultipartBody.Part.createFormData(
                        API_CONSTANTS.WEB_PARAM_PROFILE_IMAGE,
                        documentImage.name,
                        requestImageFile
                    )

                    if (imageUri != null) {
                        image = getMultipartImage(imageUri!!, context)
                    }
                }

                lateinit var response: Response<ProfileDataResponse>
                val profileDataRequest: MutableMap<String, RequestBody> = HashMap()

                profileDataRequest[API_CONSTANTS.WEB_PARAM_USER_ID] = getRequestBody(userPreference.getUserId())
                profileDataRequest[API_CONSTANTS.WEB_PARAM_USER_ROLE] = getRequestBody(userPreference.getUserRole())

                profileDataRequest[API_CONSTANTS.WEB_PARAM_FIRST_NAME] = getRequestBody(firstName)
                profileDataRequest[API_CONSTANTS.WEB_PARAM_LAST_NAME] = getRequestBody(lastName)
                profileDataRequest[API_CONSTANTS.WEB_PARAM_EMAIL] = getRequestBody(emailAddress)
                profileDataRequest[API_CONSTANTS.WEB_PARAM_MOBILE_NUMBER] = getRequestBody(phoneNumber)
                profileDataRequest[API_CONSTANTS.WEB_PARAM_ADDRESS] = getRequestBody(address)
                profileDataRequest[API_CONSTANTS.WEB_PARAM_GENDER] = getRequestBody(gender)

                profileDataRequest[API_CONSTANTS.WEB_PARAM_SERVICE_ID] = getRequestBody(service_id)
                profileDataRequest[API_CONSTANTS.WEB_PARAM_NIF_NUMBER] = getRequestBody(nif_id)
                profileDataRequest[API_CONSTANTS.WEB_PARAM_ABOUT_US] = getRequestBody(about_us)

                getNavigator()?.showLoader()

                viewModelScope.launch {
                    profileDataObservable.value = Resource.loading(null)
                    withContext(Dispatchers.IO) {
                        response = networkService.updateProfileData(profileDataRequest, image)

                    }

                    withContext(Dispatchers.Main) {
                        response.run {

                            profileDataObservable.value = baseDataSource.getResult { this }
                        }
                    }
                }
            }
        } else {
            viewModelScope.launch {
                withContext(Dispatchers.Main) {
                    profileDataObservable.value = Resource.noInternetConnection(null)
                }
            }
        }
    }

    fun getRequestBody(content: String): RequestBody {
        return RequestBody.create("multipart/form-data".toMediaTypeOrNull(), content)
    }


    fun getMultipartImage(imagePath: Uri, context: Context): MultipartBody.Part? {

        val documentImage = File(RealPathUtil.getRealPath(context, imagePath))

        if (imagePath != null) {

            val requestImageFile = RequestBody.create(
                "multipart/form-data".toMediaTypeOrNull(),
                documentImage
            )

            return MultipartBody.Part.createFormData(
                API_CONSTANTS.WEB_PARAM_PROFILE_IMAGE,
                documentImage.name,
                requestImageFile
            )

        }
        return null
    }

    fun validate(): Boolean {

        Log.e("first", "first--->$firstName")

        when {

            firstName.isEmpty() -> {
                sendError(ValidationStatus.EMPTY_FIRSTNAME)
                Log.e("first", "firstfirst--->$firstName")

            }
            !firstName.isNameValid() -> {
                sendError(ValidationStatus.TWOCHAR_FIRSTNAME)
            }
            lastName.isEmpty() -> {
                sendError(ValidationStatus.EMPTY_LASTNAME)
            }
            !lastName.isNameValid() -> {
                sendError(ValidationStatus.TWOCHAR_LASTNAME)
            }
            emailAddress.isEmpty() -> {
                sendError(ValidationStatus.EMPTYEMAIL)
            }
            !emailAddress.isEmailValid() -> {
                sendError(ValidationStatus.InvalidEmail)
            }
            phoneNumber.isEmpty() -> {
                sendError(ValidationStatus.PHONENUMBER)
            }
            !phoneNumber.isPhoneValid() -> {
                sendError(ValidationStatus.PHONENUMBERValid)
            }
            address.isEmpty() -> {
                sendError(ValidationStatus.ADDRESS_EMPTY)
            }
            nif_id.isEmpty() -> {
                sendError(ValidationStatus.NIFID)
            }
            about_us.isEmpty() -> {
                sendError(ValidationStatus.ABOUTUS)
            }
            else -> {
                return true
            }
        }
        return false
    }

    private fun sendError(error: ValidationStatus) {
        getValidationStatus().postValue(error)
    }
}