package com.assistant.top.service.provider.ui.view

import android.content.Context
import android.util.AttributeSet
import android.view.ContextThemeWrapper
import androidx.core.content.res.ResourcesCompat
import com.assistant.top.service.provider.R
import com.google.android.material.textfield.TextInputLayout

class CustomTextInputLayout : TextInputLayout {

    constructor(context: Context) : super(
        ContextThemeWrapper(
            context,
            R.style.EdittextBox
        )
    ) {
        setFontTypeFace(context)
    }

    constructor(
        context: Context,
        attrs: AttributeSet?
    ) : super(ContextThemeWrapper(context, R.style.EdittextBox), attrs) {
        setFontTypeFace(context)
    }

    constructor(
        context: Context,
        attrs: AttributeSet?,
        defStyleAttr: Int
    ) : super(
        ContextThemeWrapper(context, R.style.EdittextBox),
        attrs,
        defStyleAttr
    ) {
        setFontTypeFace(context)
    }

    private fun setFontTypeFace(context: Context) {
        val face = ResourcesCompat.getFont(context, R.font.aileron_bold)
        typeface = face
    }
}
