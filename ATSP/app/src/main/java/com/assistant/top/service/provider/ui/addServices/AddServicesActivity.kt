package com.assistant.top.service.provider.ui.addServices

import android.os.Bundle
import android.os.SystemClock
import android.util.Log
import android.view.View
import com.assistant.top.service.provider.BR
import com.assistant.top.service.provider.R
import com.assistant.top.service.provider.base.BaseActivity
import com.assistant.top.service.provider.data.Response.AddServicesResponse
import com.assistant.top.service.provider.databinding.ActivityAddServicesBinding
import com.assistant.top.service.provider.extension.logMessage
import com.assistant.top.service.provider.extension.startNewActivity
import com.assistant.top.service.provider.ui.choosePlan.ChoosePlanActivity
import com.assistant.top.service.provider.util.network.Resource
import com.assistant.top.service.provider.utility.Constants
import com.assistentetop.customer.utils.alertDialog.alertDialog
import com.assistentetop.customer.utils.alertDialog.showInternetDialog
import dagger.hilt.android.AndroidEntryPoint


@AndroidEntryPoint
class AddServicesActivity : BaseActivity<ActivityAddServicesBinding, AddServiceViewModel>(), AddServicesNavigator {

    override val layoutId: Int get() = R.layout.activity_add_services
    override val bindingVariable: Int
    get() = BR.viewmodel
    private var TAG= "Add service "
    var isInternetConnected = false
    lateinit var addServicesAdapter: AddServicesAdapter

    var servicesList : ArrayList<AddServicesResponse.ServiceTypeData>  = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


        mViewModel.setNavigator(this)
        mViewModel.setLanguageObject(language!!)
        mViewModel.addServices()

        binding.commonProgressButton.setCommonButtonListener{
            if (SystemClock.elapsedRealtime() - Constants.mLastClickTime < 1000) {
                return@setCommonButtonListener
            }
            Constants.mLastClickTime = SystemClock.elapsedRealtime()
            startNewActivity(ChoosePlanActivity::class.java)
        }
    }

    override fun onSignInNext() {

    }

    override fun showLoader() {
        binding.commonProgressButton.setLoading(true, userPreference)

    }

    fun buttonClick(v: View){
        logMessage("AddServiceActivity","clicked")
    }

    override fun setupObservable() {

        mViewModel.getAddServiceObservable().observe(this, {

            when (it.status) {
                Resource.Status.SUCCESS -> {
                    binding.commonProgressButton.setLoading(false, userPreference)

                    Log.e(TAG, "on success=>${it.message}")

                    it.let {
                        val data = gson.toJson(it.data)
                        servicesList = it.data!!.serviceTypeList
                        addServicesAdapter= AddServicesAdapter(servicesList = servicesList)
                        binding.rvAddRecyclerView.adapter = addServicesAdapter
                        Log.e("TAG", "SUCCESS::${it.status}")
                    }
                }

                Resource.Status.ERROR -> {
                    binding.commonProgressButton.setLoading(false, userPreference)

                    Log.e(TAG, "on error=>${it.message}")
                    if (!checkIsSessionOut(it.code)) {
                        it.message?.let { message ->
                            alertDialog(
                                message = if (checkIsConnectionReset(it.code)) {
                                    getString(R.string.connection_reset)
                                } else {
                                    message
                                }
                            )
                        }
                    }
                }

                Resource.Status.LOADING -> {
                    Log.e(TAG, "loading=>${it.message}")
                }

                Resource.Status.NO_INTERNET_CONNECTION -> {
                    Log.e(TAG, "no internet=>${it.message}")
                    isInternetConnected = false
                    showInternetDialog(false)
                }

                Resource.Status.SHIMMER_VIEW -> {

                }

            }
        })

    }

}