package com.assistant.top.service.provider.ui.login

import android.os.Bundle
import android.os.SystemClock
import android.text.InputFilter
import android.util.Log
import android.view.WindowManager
import com.assistant.top.service.provider.BR
import com.assistant.top.service.provider.R
import com.assistant.top.service.provider.base.BaseActivity
import com.assistant.top.service.provider.databinding.ActivityLoginBinding
import com.assistant.top.service.provider.extension.hideKeyboard
import com.assistant.top.service.provider.extension.startNewActivity
import com.assistant.top.service.provider.extension.startNewActivityWithClearTop
import com.assistant.top.service.provider.ui.forgotPassword.ForgotPassword
import com.assistant.top.service.provider.ui.home.HomeActivity
import com.assistant.top.service.provider.ui.signup.SignUpActivity
import com.assistant.top.service.provider.util.Validation
import com.assistant.top.service.provider.util.network.AppConstants.SharedPrefKey.Companion.USER_PREF
import com.assistant.top.service.provider.util.network.AppConstants.SharedPrefKey.Companion.USER_PREF_KEY
import com.assistant.top.service.provider.util.network.Resource
import com.assistant.top.service.provider.utility.Constants
import com.assistentetop.customer.utils.alertDialog.showErrorMsg502
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class LoginActivity : BaseActivity<ActivityLoginBinding, LoginViewModel>(), LoginNavigator {

    override val layoutId: Int get() = R.layout.activity_login
    override val bindingVariable: Int get() = BR.viewmodel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        whitespaceFilter()
        getWindow().setSoftInputMode(
            WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN
        );
        mViewModel.setNavigator(this)
        mViewModel.setLanguageObject(language!!)

        binding.btnSignIn.setCommonButtonListener {
            mViewModel.signin()
            //mViewModel.getNavigator()?.goToHome()
        }
    }

    private fun whitespaceFilter() {
        /*remove white space*/
        val filter =
            InputFilter { source, start, end, dest, dstart, dend ->
                for (i in start until end) {
                    if (Character.isWhitespace(source[i])) {
                        return@InputFilter ""
                    }
                }

                null
            }

        binding.etPassword.filters = arrayOf(filter)
    }


    override fun gotoForgotPassword() {
        if (SystemClock.elapsedRealtime() - Constants.mLastClickTime < 1000) {
            return
        }
        Constants.mLastClickTime = SystemClock.elapsedRealtime()
        startNewActivity(ForgotPassword::class.java)
    }

    override fun gotoSignUp() {
        if (SystemClock.elapsedRealtime() - Constants.mLastClickTime < 1000) {
            return
        }
        Constants.mLastClickTime = SystemClock.elapsedRealtime()
        startNewActivity(SignUpActivity::class.java)
    }

    override fun goToHome() {
        if (SystemClock.elapsedRealtime() - Constants.mLastClickTime < 1000) {
            return
        }
        Constants.mLastClickTime = SystemClock.elapsedRealtime()
        startNewActivity(HomeActivity::class.java, finish = true)
    }

    override fun addToCart() {

    }

    override fun showLoader() {
        binding.btnSignIn.setLoading(true, userPreference)

    }

    override fun setupObservable() {
        mViewModel.getValidationStatus().observe(this, {
            Validation.showMessageDialog(this, it)
        })
        mViewModel.getLoginObservable().observe(this, {
            when (it.status) {
                Resource.Status.SUCCESS -> {
                    binding.btnSignIn.setLoading(false, userPreference)

                    Log.e("SUCCESS", "SUCCESS::${it.status}")
                    it.let {
                        val data = gson.toJson(it.data)
                        userPreference.saveUserSession(USER_PREF, data)
                        userPreference.saveUserSession(USER_PREF_KEY, it.data!!.xApiKey)

                        Log.e("saveUserSession", "saveUserSession::${data}")
                        Log.e("saveUserSessionkey", "saveUserSessionkey::${it.data.xApiKey}")

                        goToHome()

                    }
                }
                Resource.Status.ERROR -> {
                    binding.btnSignIn.setLoading(false, userPreference)
                    if (!checkIsSessionOut(it.code)) {
                        it.message?.let { message ->
                            showErrorMsg502(it.code, language?.getLanguage(message) ?: "")
                        }
                        Log.e("ERROR", "ERROR::${it.status}")

                    }
                }

                Resource.Status.LOADING -> {
                    Log.e("LOADING", "LOADING::${it.status}")
                }
                Resource.Status.NO_INTERNET_CONNECTION -> {
                    Log.e("NO_INTERNET_CONNECTION", "NO_INTERNET_CONNECTION::${it.status}")

                }
                Resource.Status.UNKNOWN -> {

                }
                Resource.Status.SHIMMER_VIEW -> {

                }
            }
        })
    }


}