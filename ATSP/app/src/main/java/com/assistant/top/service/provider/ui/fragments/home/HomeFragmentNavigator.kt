package com.assistant.top.service.provider.ui.fragments.home

interface HomeFragmentNavigator {

    fun onItemClick(position : Int)
    fun viewDetailClicked()
}