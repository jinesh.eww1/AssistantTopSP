package com.assistant.top.service.provider.base

import android.app.Activity
import android.content.Context
import android.content.pm.ActivityInfo
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.ViewModel
import com.assistant.top.service.provider.R
import com.assistant.top.service.provider.extension.hideKeyboard
import com.assistant.top.service.provider.extension.startNewActivity
import com.assistant.top.service.provider.language.Language
import com.assistant.top.service.provider.prefs.UserPreference
import com.assistant.top.service.provider.ui.login.LoginActivity
import com.assistant.top.service.provider.ui.view.SnacyAlert
import com.assistant.top.service.provider.util.network.AppConstants
import com.assistentetop.customer.utils.alertDialog.alertDialog
import com.google.gson.Gson
import javax.inject.Inject

abstract class BaseActivity<T : ViewDataBinding, V : ViewModel> : AppCompatActivity() {
    abstract val layoutId: Int
    abstract val bindingVariable: Int

    @Inject
    lateinit var mViewModel: V
    lateinit var binding: T

    lateinit var userPreference: UserPreference
    lateinit var activity: Activity

    companion object {
        var language: Language? = null
    }

    @Inject
    lateinit var gson: Gson

    abstract fun setupObservable()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activity = this
        userPreference = UserPreference(
                activity.getSharedPreferences(
                    AppConstants.USER_PREFERENCE,
                    Context.MODE_PRIVATE
                ), activity
            )
        language = Language.newInstance(this)
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
        performDataBinding()
    }

    private fun performDataBinding() {
        activity = this
        binding = DataBindingUtil.setContentView(this, layoutId)
        binding.setVariable(bindingVariable, mViewModel)
        binding.executePendingBindings()
        setupObservable()

    }

    override fun onBackPressed() {
        super.onBackPressed()
        hideKeyboard()
    }

    open fun checkIsSessionOut(code: Int): Boolean {
        if (code == 403) {
            userPreference.clearUserSession()
            alertDialog(message = getString(R.string.session_expired), positiveClick = {
                startNewActivity(LoginActivity::class.java)
                finish()

            })
        }
        return code == 403
    }

    open fun checkIsConnectionReset(code: Int): Boolean {
        return code == 502
    }

    protected open fun goBack() {
        onBackPressed()
    }

    fun showApiError(code: Int, msg: String?) {
        if (!checkIsSessionOut(code)) {
            msg?.let { message ->
                alertDialog(
                    message = if (checkIsConnectionReset(code)) {
                        getString(R.string.connection_reset)
                    } else {
                        message
                    }
                )

            }
            Log.e("TAG", "On Error ${msg} ")
        }
    }

    fun showSuccessMsg(msg: Any) {
        val message = when (msg) {
            is Int -> {
                this.resources.getString(msg)
            }
            is String -> {
                msg
            }
            else -> {
                ""
            }
        }
        if (!message.isNullOrEmpty()) {
            SnacyAlert.create(this@BaseActivity)
                .setText(message)
                .setBackgroundColorRes(R.color.yellow)
                .setDuration(1500)
                .showIcon(true)
                .setIcon(R.drawable.ic_round_check_circle)
                .show()
        }
    }

}