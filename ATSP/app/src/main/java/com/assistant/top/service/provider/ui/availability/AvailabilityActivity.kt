package com.assistant.top.service.provider.ui.availability

import android.os.Bundle
import android.widget.CalendarView
import com.assistant.top.service.provider.BR
import com.assistant.top.service.provider.R
import com.assistant.top.service.provider.base.BaseActivity
import com.assistant.top.service.provider.databinding.ActivityAvailabilityBinding
import com.assistant.top.service.provider.extension.logMessage
import com.assistant.top.service.provider.extension.showToast
import com.assistant.top.service.provider.ui.serviceDetail.AvailablityAdapter
import dagger.hilt.android.AndroidEntryPoint
import java.text.DateFormatSymbols
import java.util.*
import kotlin.collections.ArrayList

@AndroidEntryPoint
class AvailabilityActivity : BaseActivity<ActivityAvailabilityBinding, AvailabilityViewModel>(),
    AvailabilityNavigator {

    override val layoutId: Int get() = R.layout.activity_availability
    override val bindingVariable: Int get() = BR.viewmodel
    private var availableDetailListing: ArrayList<AvailabilityViewModel> = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding.rvAvalibilityList.adapter = AvailablityAdapter(this, availableDetailListing)

        binding.calanderview.setOnDateChangeListener { view, year, month, _ ->
            binding.txtMonthYear.text = getMonth(month).plus(" ").plus(year)
        }



    }

    fun getMonth(month: Int): String? {
        return DateFormatSymbols().months[month - 1]
    }

    override fun setupObservable() {

    }
}