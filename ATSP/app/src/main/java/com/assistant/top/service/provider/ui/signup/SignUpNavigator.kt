package com.assistant.top.service.provider.ui.signup

interface SignUpNavigator {

    fun onClickNext()
    fun signIn()
    fun showLoader()

}