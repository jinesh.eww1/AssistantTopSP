package com.assistant.top.service.provider.ui.edit_service

interface EditServiceNavigator {

    fun onClickGenralService()
    fun onClickAppointmentService()
    fun onTabChange(position: Int)

}