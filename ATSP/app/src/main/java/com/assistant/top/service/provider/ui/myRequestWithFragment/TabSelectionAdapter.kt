package com.assistant.top.service.provider.ui.myRequestWithFragment

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.assistant.top.service.provider.R
import com.assistant.top.service.provider.databinding.MyRequestTabListBinding
import com.assistant.top.service.provider.extension.setRectDrawable

class TabSelectionAdapter(
    val navigator: MyRequestFragmentNavigator?,
    val tabList: ArrayList<String>,
    val position: Int
) : RecyclerView.Adapter<TabSelectionAdapter.MenuHolder>() {

    private var selectedPosition = position
    lateinit var context: Context
    lateinit var binding: MyRequestTabListBinding

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MenuHolder {

        context = parent.context
        binding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.my_request_tab_list,
            parent,
            false
        )

        return MenuHolder(binding)
    }

    @SuppressLint("NotifyDataSetChanged", "ResourceAsColor")
    override fun onBindViewHolder(holder: MenuHolder, @SuppressLint("RecyclerView") position: Int) {

        if (selectedPosition == position) {
            holder.binding.txtTab.setRectDrawable(
                90f,
                ContextCompat.getColor(
                    context,
                    R.color.theme
                )
            )

            holder.binding.txtTab.setTextColor(
                ContextCompat.getColor(
                    context,
                    R.color.white
                )
            )

        } else {
            holder.binding.txtTab.setBackgroundColor(
                ContextCompat.getColor(
                    context,
                    R.color.white
                )
            )

            holder.binding.txtTab.setTextColor(
                ContextCompat.getColor(
                    context,
                    R.color.my_request_color
                )
            )
        }

        holder.binding.txtTab.text = tabList[position]

        holder.itemView.setOnClickListener {
            selectedPosition = position
            notifyDataSetChanged()
            navigator?.onItemClick(position)

        }
    }

    override fun getItemCount(): Int {
        return tabList.size
    }

    fun scrollTo(position: Int) {
        selectedPosition = position
        notifyDataSetChanged()
    }

    class MenuHolder(var binding: MyRequestTabListBinding) : RecyclerView.ViewHolder(binding.root)
}