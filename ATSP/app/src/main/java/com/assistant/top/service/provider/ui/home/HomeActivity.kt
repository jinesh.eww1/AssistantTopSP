package com.assistant.top.service.provider.ui.home

import android.annotation.SuppressLint
import android.graphics.Color
import android.os.Bundle
import android.os.CountDownTimer
import android.os.SystemClock
import android.util.Log
import android.view.View
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.widget.Toolbar
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import com.assistant.top.service.provider.BR
import com.assistant.top.service.provider.R
import com.assistant.top.service.provider.base.BaseActivity
import com.assistant.top.service.provider.databinding.ActivityHomeBinding
import com.assistant.top.service.provider.extension.*
import com.assistant.top.service.provider.ui.fragments.complain.ComplainFragment
import com.assistant.top.service.provider.ui.fragments.home.HomeFragment
import com.assistant.top.service.provider.ui.fragments.manageAvalibility.ManageAvailabilityFragment
import com.assistant.top.service.provider.ui.fragments.myServices.MyServicesFragment
import com.assistant.top.service.provider.ui.fragments.profile.ProfileFragment
import com.assistant.top.service.provider.ui.fragments.settings.SettingsFragment
import com.assistant.top.service.provider.ui.login.LoginActivity
import com.assistant.top.service.provider.ui.notification.NotificationActivity
import com.assistant.top.service.provider.util.AppLog
import com.assistant.top.service.provider.util.Validation
import com.assistant.top.service.provider.util.network.Resource
import com.assistant.top.service.provider.utility.Constants
import com.assistentetop.customer.utils.alertDialog.showInternetDialog
import com.assistentetop.customer.utils.alertDialog.showSuccessAlert
import dagger.hilt.android.AndroidEntryPoint


@AndroidEntryPoint
class HomeActivity : BaseActivity<ActivityHomeBinding, HomeViewModel>(), HomeActivityNavigator {


    lateinit var nav: HomeActivityNavigator
    private lateinit var menuAdapter: MenuAdapter
    override val layoutId: Int get() = R.layout.activity_home
    override val bindingVariable: Int get() = BR.viewmodel
    val menuList = ArrayList<MenuItemModel>()

    private var homeFragment = HomeFragment()
    private var myServiceFragment = MyServicesFragment()
    private var profileFragment = ProfileFragment()
    private var settingsFragment = SettingsFragment()
    private var manageAvailabilityFragment = ManageAvailabilityFragment()
    private var complainFragment = ComplainFragment()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        nav = this
        initt()
        setMenuAdapter()
        setUpDrawer()

        AppLog.e(TAG,"userId=> ${userPreference.getUserId()}")
        AppLog.e(TAG,"userName=> ${userPreference.getUserName()}")

        binding.userProfileIcon.setOnClickListener {
            updateToolBar(
                getString(R.string.profile),
                isShowNotification = true,
                notificationIcon = R.drawable.ic_edit_icon
            )
            setCurrentFragment(profileFragment)
            closeDrawer()
        }

        binding.commonToolbar.setToolbarBackListener {

            if (SystemClock.elapsedRealtime() - Constants.mLastClickTime < 1000) {
                return@setToolbarBackListener
            }
            Constants.mLastClickTime = SystemClock.elapsedRealtime()

            val currentFragment: Fragment? = supportFragmentManager.findFragmentByTag("_")
            if (currentFragment is HomeFragment) {
                openDrawer()
            } else {
                hideKeyboard()
                supportFragmentManager.popBackStack()
                updateToolBar(
                    getString(R.string.home),
                    R.drawable.menu_icon,
                    true,
                    R.drawable.ball_icon
                )
            }
        }

        binding.commonToolbar.binding.ivNotification.setOnClickListener {

            if (SystemClock.elapsedRealtime() - Constants.mLastClickTime < 1000) {
                return@setOnClickListener
            }

            Constants.mLastClickTime = SystemClock.elapsedRealtime()

            val currentFragment: Fragment? = supportFragmentManager.findFragmentByTag("_")
            if (currentFragment is ProfileFragment) {

                currentFragment.onProfileNavigator.onClickEditButton()
                binding.commonToolbar.binding.ivNotification.invisible()

            } else {

                startNewActivity(NotificationActivity::class.java)
            }
        }
    }

    fun setUserProfileData() {
        binding.tvUserName.text = userPreference.getUserName()
        binding.tvUserEmail.text = userPreference.getEmailAddress()
        binding.userProfileIcon.loadImage(userPreference.getUserImage())
        Log.e("imgUser", "imgUser")

    }

    override fun onBackPressed() {

        if (binding.llDrawerLayout.isDrawerOpen(GravityCompat.START)) {
            binding.llDrawerLayout.closeDrawer(GravityCompat.START)
        } else {

            val currentFragment: Fragment? = supportFragmentManager.findFragmentByTag("_")
            if (currentFragment is HomeFragment) {
                finish()
            } else {

                supportFragmentManager.popBackStack()
                updateToolBar(
                    getString(R.string.home),
                    R.drawable.menu_icon,
                    true,
                    R.drawable.ball_icon
                )
            }
        }
    }

    private fun initt() {
        mViewModel.setNavigator(this)
        setCurrentFragment(homeFragment)
    }


    private fun setMenuAdapter() {

        menuList.apply {
            add(
                MenuItemModel(
                    getString(R.string.home),
                    R.drawable.ic_home_unselected
                )
            )
            add(
                MenuItemModel(
                    getString(R.string.my_services),
                    R.drawable.ic_my_service_unselected
                )
            )
            add(
                MenuItemModel(
                    getString(R.string.profile),
                    R.drawable.ic_user_unselected
                )
            )
            add(
                MenuItemModel(
                    getString(R.string.settings),
                    R.drawable.ic_settings_unselected
                )
            )
            add(
                MenuItemModel(
                    getString(R.string.manage_availability),
                    R.drawable.ic_manage_avaibility_unselected
                )
            )
            add(
                MenuItemModel(
                    getString(R.string.complain),
                    R.drawable.ic_manage_avaibility_unselected
                )
            )
        }

        menuAdapter = MenuAdapter(menuList, this)
        binding.rvMenu.adapter = menuAdapter
    }

    @SuppressLint("NotifyDataSetChanged")
    private fun openDrawer() {
        setUserProfileData()
        binding.layMain.radius = 25f
        Constants.CURRENT_POSITION = 0
        menuAdapter.notifyDataSetChanged()
        binding.llDrawerLayout.openDrawer(binding.rlDrawer)
    }

    private fun closeDrawer() {
        binding.layMain.radius = 0f
        binding.llDrawerLayout.closeDrawer(binding.rlDrawer)
    }

    private fun setUpDrawer() {

        val toolbar = Toolbar(this)
        setSupportActionBar(toolbar)
        val actionBarDrawerToggle: ActionBarDrawerToggle = object :
            ActionBarDrawerToggle(
                this,
                binding.llDrawerLayout,
                toolbar,
                R.string.openDrawer,
                R.string.closeDrawer
            ) {
            override fun onDrawerSlide(
                drawerView: View,
                slideOffset: Float
            ) {
                super.onDrawerSlide(drawerView, slideOffset)
                val scaleFactor = 6f
                val slideX = drawerView.width * slideOffset
                binding.layMain.translationX = slideX
                binding.layMain.scaleX = 1 - slideOffset / scaleFactor
                binding.layMain.scaleY = 1 - slideOffset / scaleFactor

            }

            override fun onDrawerClosed(drawerView: View) {
                super.onDrawerClosed(drawerView)
                binding.layMain.radius = 0f
                invalidateOptionsMenu()
            }

            override fun onDrawerOpened(drawerView: View) {
                super.onDrawerOpened(drawerView)
                invalidateOptionsMenu()
            }
        }

        binding.llDrawerLayout.setScrimColor(Color.TRANSPARENT)
        binding.llDrawerLayout.drawerElevation = 0f
        binding.llDrawerLayout.addDrawerListener(actionBarDrawerToggle)
        binding.llDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED)

        actionBarDrawerToggle.syncState()

    }

    override fun onClickDrawerItem(position: Int) {
        closeDrawer()

        when (position) {
            0 -> {
                setCurrentFragment(homeFragment)
            }
            1 -> {
                updateToolBar(getString(R.string.my_services))
                setCurrentFragment(myServiceFragment)
            }
            2 -> {
                updateToolBar(
                    getString(R.string.profile),
                    isShowNotification = true,
                    notificationIcon = R.drawable.ic_edit_icon
                )
                setCurrentFragment(profileFragment)
            }
            3 -> {
                updateToolBar(getString(R.string.settings))
                setCurrentFragment(settingsFragment)
            }
            4 -> {
                updateToolBar(getString(R.string.working_hours))
                setCurrentFragment(manageAvailabilityFragment)
            }
            5 -> {
                updateToolBar(getString(R.string.complain))
                setCurrentFragment(complainFragment)
            }

        }
    }

    override fun onProfileFragmentClicks() {

    }

    override fun onLogout() {
        if (SystemClock.elapsedRealtime() - Constants.mLastClickTime < 1000) {
            return
        }
        Constants.mLastClickTime = SystemClock.elapsedRealtime()
        mViewModel.logout()
    }

    private fun updateToolBar(
        title: String,
        image: Int = R.drawable.white_back_icon,
        isShowNotification: Boolean = false,
        notificationIcon: Int = R.drawable.ic_edit_icon
    ) {

        binding.commonToolbar.binding.tvTitle.text = title
        binding.commonToolbar.binding.ivBack.setImageResource(image)
        if (isShowNotification) {
            binding.commonToolbar.binding.ivNotification.visible()
            binding.commonToolbar.binding.ivNotification.setImageResource(notificationIcon)
        } else {
            binding.commonToolbar.binding.ivNotification.invisible()
        }

    }

    private fun setCurrentFragment(fragment: Fragment, tag: String = "_") {
        supportFragmentManager.beginTransaction().apply {
            replace(binding.fragmentContainer.id, fragment, tag)
            addToBackStack(tag)
            commit()
        }
    }

    override fun setupObservable() {
        setUserProfileData()
        mViewModel.getValidationStatus().observe(this, {
            Validation.showMessageDialog(this, it)
        })
        mViewModel.getLogoutObservable().observe(this, {
            when (it.status) {
                Resource.Status.SUCCESS -> {
//                    hideLoaderDialog()
                    Log.e("SUCCESS", "SUCCESSSUCCESS::${it.status}")
                    it.message.let { message ->
                        showSuccessAlert(language?.getLanguage(message) ?: "")

                        val timer = object : CountDownTimer(2500, 1000) {
                            override fun onTick(millisUntilFinished: Long) {

                            }

                            override fun onFinish() {
                                userPreference.clearUserSession()
                                startNewActivity(LoginActivity::class.java,finish = true)
                            }
                        }
                        timer.start()
                    }
                }

                Resource.Status.ERROR -> {
                    Log.e("error", "error::${it.status}")
                    showApiError(it.code, it.message)
                }

                Resource.Status.LOADING -> {
                }

                Resource.Status.NO_INTERNET_CONNECTION -> {
                    showInternetDialog(
                        false
                    )
                }
            }
        })
    }
}