package com.assistant.top.service.provider.ui.choosePlan

import android.os.Bundle
import android.os.SystemClock
import android.util.Log
import com.assistant.top.service.provider.BR
import com.assistant.top.service.provider.R
import com.assistant.top.service.provider.base.BaseActivity
import com.assistant.top.service.provider.data.Response.ChoosePlanResponse
import com.assistant.top.service.provider.databinding.ActivityChoosePlanBinding
import com.assistant.top.service.provider.extension.startNewActivityWithClearTop
import com.assistant.top.service.provider.ui.home.HomeActivity
import com.assistant.top.service.provider.util.network.Resource
import com.assistant.top.service.provider.utility.Constants
import com.assistentetop.customer.utils.alertDialog.alertDialog
import com.assistentetop.customer.utils.alertDialog.showInternetDialog
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ChoosePlanActivity : BaseActivity<ActivityChoosePlanBinding,ChoosePlanViewModel>(),ChoosePlanNavigator {

    override val layoutId: Int get() = R.layout.activity_choose_plan
    override val bindingVariable: Int get() = BR.viewmodel
    private var TAG= "Add service "
    var isInternetConnected = false
    lateinit var choosePlanAdapter: ChoosePlanAdapter
    var navigator: ChoosePlanNavigator? = null
    var choosePlanList : ArrayList<ChoosePlanResponse.plansData>  = ArrayList()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mViewModel.choosePlan()
        navigator = this
        mViewModel.setNavigator(this)
        mViewModel.setLanguageObject(language!!)

        binding.commonProgressButton.setCommonButtonListener {
            if (SystemClock.elapsedRealtime() - Constants.mLastClickTime < 1000) {
                return@setCommonButtonListener
            }
            Constants.mLastClickTime = SystemClock.elapsedRealtime()
            startNewActivityWithClearTop(HomeActivity::class.java,finish = true)
        }
    }

    override fun onItemClick() {

    }

    override fun showLoader() {
        binding.commonProgressButton.setLoading(true, userPreference)

    }

    override fun setupObservable() {

        mViewModel.getchoosePlanObservable().observe(this, {

            when (it.status) {
                Resource.Status.SUCCESS -> {
                    binding.commonProgressButton.setLoading(false, userPreference)

                    Log.e(TAG, "on success=>${it.message}")

                    it.let {
                        val data = gson.toJson(it.data)
                        choosePlanList = it.data!!.plansList
                        choosePlanAdapter= ChoosePlanAdapter(navigator,choosePlanList, language)
                        binding.rvServiceBox.adapter = choosePlanAdapter
                        Log.e("TAG", "SUCCESS::${it.status}")
                    }
                }

                Resource.Status.ERROR -> {
                    binding.commonProgressButton.setLoading(false, userPreference)

                    Log.e(TAG, "on error=>${it.message}")
                    if (!checkIsSessionOut(it.code)) {
                        it.message?.let { message ->
                            alertDialog(
                                message = if (checkIsConnectionReset(it.code)) {
                                    getString(R.string.connection_reset)
                                } else {
                                    message
                                }
                            )
                        }
                    }
                }

                Resource.Status.LOADING -> {
                    Log.e(TAG, "loading=>${it.message}")
                }

                Resource.Status.NO_INTERNET_CONNECTION -> {
                    Log.e(TAG, "no internet=>${it.message}")
                    isInternetConnected = false
                    showInternetDialog(false)
                }

                Resource.Status.SHIMMER_VIEW -> {

                }

            }
        })
    }
}