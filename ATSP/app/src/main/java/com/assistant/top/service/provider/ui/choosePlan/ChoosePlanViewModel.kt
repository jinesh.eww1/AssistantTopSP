package com.assistant.top.service.provider.ui.choosePlan

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.assistant.top.service.provider.base.BaseViewModel
import com.assistant.top.service.provider.data.Response.AddServicesResponse
import com.assistant.top.service.provider.data.Response.ChoosePlanResponse
import com.assistant.top.service.provider.prefs.UserPreference
import com.assistant.top.service.provider.util.AppLog
import com.assistant.top.service.provider.util.network.API_CONSTANTS
import com.assistant.top.service.provider.util.network.NetworkService
import com.assistant.top.service.provider.util.network.NetworkUtils
import com.assistant.top.service.provider.util.network.Resource
import com.assistant.top.service.provider.utility.Constants
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.Response
import javax.inject.Inject

class ChoosePlanViewModel @Inject constructor(
    var context: Context,
    var userPreference: UserPreference,
    var networkService: NetworkService
) : BaseViewModel<ChoosePlanNavigator>(){

    private val choosePlanResponseObservable: MutableLiveData<Resource<ChoosePlanResponse>> =
        MutableLiveData()

    fun getchoosePlanObservable(): LiveData<Resource<ChoosePlanResponse>> {
        return choosePlanResponseObservable
    }

    fun choosePlan(){
        if (NetworkUtils.isNetworkConnected(context)) {
            var response: Response<ChoosePlanResponse>? = null
            viewModelScope.launch {
                choosePlanResponseObservable.value = Resource.loading(null)
                withContext(Dispatchers.IO) {
                    AppLog.e(Constants.TAG, "$response")
                    response = networkService.choosePlan(API_CONSTANTS.WEB_SERVICE_CHOOSE_PLAN)
                }
                withContext(Dispatchers.Main) {
                    response.run {
                        AppLog.e(Constants.TAG, "${baseDataSource.getResult(true) { this!! }}")
                        choosePlanResponseObservable.value = baseDataSource.getResult(true) { this!! }
                    }
                }
            }
        } else {
            viewModelScope.launch {
                withContext(Dispatchers.Main) {
                    choosePlanResponseObservable.value = Resource.noInternetConnection(null)
                }
            }
        }
    }

}