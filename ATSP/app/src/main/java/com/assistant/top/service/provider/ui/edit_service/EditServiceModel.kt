package com.assistant.top.service.provider.ui.edit_service

import com.assistant.top.service.provider.base.BaseViewModel
import javax.inject.Inject

class EditServiceModel @Inject constructor() : BaseViewModel<EditServiceNavigator>(){

    fun onClickGeneralService(){
        getNavigator()?.onClickGenralService()
    }

    fun onClickAppointmentService(){
        getNavigator()?.onClickAppointmentService()
    }

}