package com.assistant.top.service.provider.ui.fragments.manageAvalibility

import android.os.Bundle
import android.os.Handler
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import com.assistant.top.service.provider.BR
import com.assistant.top.service.provider.R
import com.assistant.top.service.provider.base.BaseFragment
import com.assistant.top.service.provider.databinding.FragmentManageAvailabilityBinding
import com.assistant.top.service.provider.extension.gone
import com.assistant.top.service.provider.extension.startNewActivity
import com.assistant.top.service.provider.extension.visible
import com.assistant.top.service.provider.model.TimingList
import com.assistant.top.service.provider.model.WorkingHoursModel
import com.assistant.top.service.provider.ui.availability.AvailabilityActivity
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ManageAvailabilityFragment :
    BaseFragment<FragmentManageAvailabilityBinding, ManageAvailabilityViewModel>(),
    ManageAvailabilityNavigator, AdapterView.OnItemSelectedListener {

    override val layoutId: Int get() = R.layout.fragment_manage_availability
    override val bindingVariable: Int get() = BR.viewmodel

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mViewModel.setNavigator(this)

        val adapter = context?.let {
            ArrayAdapter.createFromResource(
                it,
                R.array.hour_count,
                R.layout.signup_spinner_item
            )
        }
        if (adapter != null) {
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        }
        binding.spinner.adapter = adapter
        binding.spinner.onItemSelectedListener = this

        binding.tvHour.setOnClickListener {
            binding.spinner.performClick()
        }

        val workingHoursData = ArrayList<WorkingHoursModel>()
        workingHoursData.apply {

            this.add(WorkingHoursModel("Sun ", getTimeSlots()))
            this.add(WorkingHoursModel("Mon ", getTimeSlots()))
            this.add(WorkingHoursModel("Tue ", getTimeSlots()))
            this.add(WorkingHoursModel("Wed ", getTimeSlots()))
            this.add(WorkingHoursModel("Thu ", getTimeSlots()))
            this.add(WorkingHoursModel("Fri ", getTimeSlots()))

        }

        Handler().postDelayed({
            binding.rvWorkingHours.adapter =
                ManageAvailabilityAdapter(requireActivity(), workingHoursData)

        }, 250)

    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        val text: String = parent?.getItemAtPosition(position).toString()
        binding.tvhour.text = text
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {

    }

    private fun getTimeSlots(): java.util.ArrayList<TimingList> {
        val list = arrayListOf<TimingList>()
        list.add(TimingList("08:00", "12:00", false))
        list.add(TimingList("09:00", "10:00", false))

        return list
    }

    override fun onClickDayChange() {
        requireActivity().startNewActivity(AvailabilityActivity::class.java)
    }

    override fun setupObservable() {

    }
}