package com.assistant.top.service.provider.ui.signup

import android.Manifest
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import com.assistant.top.service.provider.R
import com.assistant.top.service.provider.BR
import com.assistant.top.service.provider.base.BaseActivity
import com.assistant.top.service.provider.databinding.ActivitySignUpBinding
import com.assistant.top.service.provider.extension.startNewActivity
import com.assistant.top.service.provider.ui.addServices.AddServicesActivity
import dagger.hilt.android.AndroidEntryPoint
import android.annotation.SuppressLint
import android.app.Activity
import android.app.AlertDialog
import android.content.ContentValues
import android.content.Context
import android.content.DialogInterface
import android.content.pm.PackageManager
import android.database.Cursor
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.SystemClock
import android.provider.MediaStore
import android.provider.Settings
import android.text.InputFilter
import android.util.Log
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.assistant.top.service.provider.extension.startNewActivityWithClearTop
import com.assistant.top.service.provider.extension.visible
import com.assistant.top.service.provider.ui.home.HomeActivity
import com.assistant.top.service.provider.util.AppLog
import com.assistant.top.service.provider.util.Validation
import com.assistant.top.service.provider.util.network.AppConstants
import com.assistant.top.service.provider.util.network.Resource
import com.assistant.top.service.provider.utility.Constants
import com.assistentetop.customer.utils.alertDialog.showErrorMsg502
import com.google.android.gms.location.places.ui.PlacePicker.RESULT_ERROR
import com.google.android.libraries.places.api.Places
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.widget.Autocomplete
import com.google.android.libraries.places.widget.AutocompleteActivity
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode
import java.io.File

@AndroidEntryPoint
class SignUpActivity : BaseActivity<ActivitySignUpBinding, SignUpViewModel>(), SignUpNavigator,
    AdapterView.OnItemSelectedListener {

    override val layoutId: Int
        get() = R.layout.activity_sign_up
    override val bindingVariable: Int
        get() = BR.viewmodel

    private lateinit var place: Place
    private var DestinationAddress: String = ""

    private var TAG = "SplashActivity"
    val REQUEST_ID_MULTIPLE_PERMISSIONS = 101

    private val REQUEST_IMAGE_CAPTURE = 0
    private val REQUEST_PICK_IMAGE = 1
    private val REQUEST_PLACE_PICKER_ADDRESS = 2

    private var mCurrentPhotoPath: Uri? = null
    var photoFile: File? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mViewModel.setNavigator(this)
        mViewModel.setLanguageObject(language!!)

        genderSpinnerData()
        whitespaceFilter()
        isCurrentLocationAddress()

        binding.imgUser.setOnClickListener {
            if (checkAndRequestPermissions(this)) {
                chooseImage(this)
            }
        }

        binding.tvGender.setOnClickListener {
            binding.spinner.performClick()
        }

        binding.btnSignUp.setCommonButtonListener {
            mViewModel.signUp()
            //mViewModel.getNavigator()?.onClickNext()
        }

        binding.etPassword.filters += InputFilter.LengthFilter(20)


    }

    private fun isCurrentLocationAddress() {
        binding.etAddress.setOnClickListener {
            val fields =
                listOf(Place.Field.ID, Place.Field.NAME, Place.Field.ADDRESS, Place.Field.LAT_LNG)
            val intent =
                Autocomplete.IntentBuilder(AutocompleteActivityMode.FULLSCREEN, fields).build(this)
            startActivityForResult(intent, REQUEST_PLACE_PICKER_ADDRESS)
            Places.initialize(applicationContext, getString(R.string.google_maps_key))
        }
    }

    private fun whitespaceFilter() {
        /*remove white space*/
        val filter =
            InputFilter { source, start, end, dest, dstart, dend ->
                for (i in start until end) {
                    if (Character.isWhitespace(source[i])) {
                        return@InputFilter ""
                    }
                }

                null
            }

        binding.etPassword.filters = arrayOf(filter)
    }


    private fun genderSpinnerData() {
        val adapter =
            ArrayAdapter.createFromResource(this, R.array.gender, R.layout.signup_spinner_item)
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        binding.spinner.adapter = adapter
        binding.spinner.onItemSelectedListener = this
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

        binding.spinner.adapter = adapter
        binding.spinner.onItemSelectedListener = this

    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {

        val text: String = parent?.getItemAtPosition(position).toString()
        mViewModel.gender = text
        binding.tvGender.text = text
        if (binding.spinner.selectedItemPosition < 0) {
            mViewModel.sendError(error(R.string.please_select_atleast_one_gender))
        }
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {
    }

    override fun onClickNext() {
        if (SystemClock.elapsedRealtime() - Constants.mLastClickTime < 1000) {
            return
        }
        Constants.mLastClickTime = SystemClock.elapsedRealtime()
        startNewActivity(AddServicesActivity::class.java)
    }

    override fun signIn() {
        onBackPressed()
    }

    override fun showLoader() {
        binding.btnSignUp.setLoading(true, userPreference)

    }

    override fun setupObservable() {
        Log.e("TAG", "getobserver")
        mViewModel.getValidationStatus().observe(this, {
            Validation.showMessageDialog(this, it)

        })
        mViewModel.getSignUpObservable().observe(this, {
            when (it.status) {
                Resource.Status.SUCCESS -> {
                    binding.btnSignUp.setLoading(false, userPreference)

                    it.let {
                        val data = gson.toJson(it.data)
                        userPreference.saveUserSession(AppConstants.SharedPrefKey.USER_PREF, data)
                        userPreference.saveUserSession(
                            AppConstants.SharedPrefKey.USER_PREF_KEY,
                            it.data!!.xApiKey
                        )
                        startNewActivityWithClearTop(AddServicesActivity::class.java, finish = true)
                        Log.e("TAG", "SUCCESS::${it.status}")
                    }
                }


                Resource.Status.ERROR -> {
                    //showMobileAlreadyRegister()
                    binding.btnSignUp.setLoading(false, userPreference)
                    if (!checkIsSessionOut(it.code)) {
                        it.message?.let { message ->
                            showErrorMsg502(it.code, language?.getLanguage(message) ?: "")
                        }
                        Log.e("TAG", "On Error ${it.message} ")
                    }

                }
                Resource.Status.LOADING -> {
                    Log.e("LOADING", "LOADING::${it.status}")

                }
                Resource.Status.NO_INTERNET_CONNECTION -> {
                    Log.e("NO_INTERNET_CONNECTION", "NO_INTERNET_CONNECTION::${it.status}")

                }
                Resource.Status.UNKNOWN -> {
                    //Log.e("signin", "signin::${viewDataBinding.btnSignIn}")

                }
                Resource.Status.SHIMMER_VIEW -> {
                    //Log.e("signin", "signin::${viewDataBinding.btnSignIn}")

                }
            }
        })

    }


    @SuppressLint("SimpleDateFormat")
    private fun chooseImage(context: Context) {
        val optionsMenu = arrayOf<CharSequence>(
            getString(R.string.take_photo),
            getString(R.string.Choose_from_Gallery),
            getString(R.string.Exit)
        )
        val builder: AlertDialog.Builder = AlertDialog.Builder(context)
        builder.setItems(optionsMenu, DialogInterface.OnClickListener { dialogInterface, i ->
            if (optionsMenu[i] == getString(R.string.take_photo)) {

                val values = ContentValues(1)
                mCurrentPhotoPath = activity.contentResolver.insert(
                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                    values
                )
                val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                cameraIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION or Intent.FLAG_GRANT_WRITE_URI_PERMISSION)
                cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, mCurrentPhotoPath)
                startActivityForResult(cameraIntent, REQUEST_IMAGE_CAPTURE)

            } else if (optionsMenu[i] == getString(R.string.Choose_from_Gallery)) {
                val pickPhoto =
                    Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
                startActivityForResult(pickPhoto, REQUEST_PICK_IMAGE)

            } else if (optionsMenu[i] == getString(R.string.cancel)) {
                dialogInterface.dismiss()
            }
        })
        builder.show()
    }

    fun checkAndRequestPermissions(context: Activity?): Boolean {
        val WExtstorePermission = ContextCompat.checkSelfPermission(
            context!!,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
        )
        val cameraPermission = ContextCompat.checkSelfPermission(
            context,
            Manifest.permission.CAMERA
        )
        val listPermissionsNeeded: MutableList<String> = ArrayList()
        if (cameraPermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.CAMERA)
        }


        if (WExtstorePermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded
                .add(Manifest.permission.WRITE_EXTERNAL_STORAGE)
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(
                context, listPermissionsNeeded
                    .toTypedArray(),
                REQUEST_ID_MULTIPLE_PERMISSIONS
            )
            return false
        }
        return true
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            REQUEST_ID_MULTIPLE_PERMISSIONS ->
                if (ContextCompat.checkSelfPermission(
                        this,
                        Manifest.permission.CAMERA
                    ) != PackageManager.PERMISSION_GRANTED
                ) {
                    val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
                    val uri = Uri.fromParts("package", packageName, null)
                    intent.data = uri
                    startActivity(intent)
                } else if (ContextCompat.checkSelfPermission(
                        this,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE
                    ) != PackageManager.PERMISSION_GRANTED
                ) {
                    /* val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
                     val uri = Uri.fromParts("package", packageName, null)
                     intent.data = uri
                     startActivity(intent)*/
                } else {
                    chooseImage(this)
                }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode != RESULT_CANCELED) {
            AppLog.e(TAG,"requestCode => $requestCode")
            AppLog.e(TAG,"resultcode => $resultCode")
            when (requestCode) {
                REQUEST_IMAGE_CAPTURE ->

                if (resultCode == RESULT_OK) {
                        val pathCol = arrayOf(MediaStore.Images.Media.DATA)
                        val cursor: Cursor? = mCurrentPhotoPath?.let {
                            activity.contentResolver.query(
                                it,
                                pathCol,
                                null,
                                null,
                                null
                            )
                        }
                        cursor?.moveToFirst()
                        var bmp: Bitmap
                        val colIdx: Int = cursor?.getColumnIndex(pathCol[0])!!
                        val img: String = cursor.getString(colIdx)
                        Log.e("imgTest", "" + img)
                        cursor.close()
                        bmp = BitmapFactory.decodeFile(img)

                        binding.imgUser.setImageBitmap(bmp)
                        mViewModel.imageUri = mCurrentPhotoPath

                    }
                REQUEST_PICK_IMAGE -> {
                    if (resultCode == RESULT_OK && data != null) {
                        val selectedImage: Uri? = data.data
                        binding.imgUser.setImageURI(selectedImage)
                        mViewModel.imageUri = selectedImage
                    }
                }
                REQUEST_PLACE_PICKER_ADDRESS -> {
                    if (resultCode == RESULT_OK) {
                        data?.let {
                            place = Autocomplete.getPlaceFromIntent(data)
                            Log.i(
                                TAG,
                                "Place: ${place.name}, ${place.id}, ${place.address}, ${place.latLng}"
                            )
                            binding.etAddress.setText(place.address)
                            DestinationAddress = place.address

                        }
                    } else if (resultCode == RESULT_ERROR) {
                        data?.let {
                            val status = Autocomplete.getStatusFromIntent(data)
                            Log.i(TAG, status.statusMessage)
                        }
                    }
                }

                else -> {
                    Log.e("camara", "another data $requestCode")
                }
            }
        }
    }

}

