package com.assistant.top.service.provider.ui.fragments.manageAvalibility

import com.assistant.top.service.provider.base.BaseViewModel
import javax.inject.Inject

class ManageAvailabilityViewModel @Inject constructor() : BaseViewModel<ManageAvailabilityNavigator>() {

    fun onClickDayChange(){
        getNavigator()?.onClickDayChange()
    }

}