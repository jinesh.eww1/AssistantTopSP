package com.assistant.top.service.provider.ui.service_details

import com.assistant.top.service.provider.base.BaseViewModel
import javax.inject.Inject

class ServiceDetailstViewModel @Inject constructor() : BaseViewModel<ServiceDetailsNavigator>() {

    fun onClickSendMessage(){
        getNavigator()?.onClickSendMessage()
    }

}