package com.assistant.top.service.provider.ui.signup

import android.content.Context
import android.net.Uri
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.assistant.top.service.provider.base.BaseViewModel
import com.assistant.top.service.provider.data.Response.LoginResponse
import com.assistant.top.service.provider.prefs.UserPreference
import com.assistant.top.service.provider.util.*
import com.assistant.top.service.provider.util.file.RealPathUtil.getRealPath
import com.assistant.top.service.provider.util.network.API_CONSTANTS
import com.assistant.top.service.provider.util.network.API_CONSTANTS.Companion.WEB_PARAM_PROFILE_IMAGE
import com.assistant.top.service.provider.util.network.NetworkService
import com.assistant.top.service.provider.util.network.Resource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Response
import java.io.File
import javax.inject.Inject

class SignUpViewModel @Inject constructor(
    var context: Context,
    var userPreference: UserPreference,
    var networkService: NetworkService
) : BaseViewModel<SignUpNavigator>() {

    private val TAG: String = "SignUpScreen"
    var firstName: String = ""
    var lastName: String = ""
    var emailAddress: String = ""
    var phoneNumber: String = ""
    var address: String = ""
    var password: String = ""
    var gender = userPreference.getUserGender()
    var imageUri: Uri? = null
    var fcm_device_token = "66676384312SDGSDGSDGSDGB"
    var plan_id = "2"
    var service_id = "2"

    private val signUpResponseObservable: MutableLiveData<Resource<LoginResponse>> =
        MutableLiveData()

    fun getSignUpObservable(): LiveData<Resource<LoginResponse>> {
        return signUpResponseObservable
    }

    private val _validationState = MutableLiveData("")
    val validationStat = _validationState

    fun onClickSignIn() {
        getNavigator()?.signIn()
    }

    fun signUp() {
        if (NetworkUtils.isNetworkConnected(context)) {
            Log.e(TAG, "signUp")

            if (validateSignUp()) {
                Log.e(TAG, "validateSignUp=>} ")

                getNavigator()?.showLoader()

                lateinit var response: Response<LoginResponse>
                var image: MultipartBody.Part? = null
                val sgnUpRequest: MutableMap<String, RequestBody> = HashMap()

                if(imageUri!=null){

                    val documentImage = File(getRealPath(context, imageUri!!))
                    val requestImageFile = RequestBody.create(
                        "multipart/form-data".toMediaTypeOrNull(),
                        documentImage
                    )

                    image = MultipartBody.Part.createFormData(
                        WEB_PARAM_PROFILE_IMAGE,
                        documentImage.name,
                        requestImageFile
                    )

                    if (imageUri != null) {
                        image = getMultipartImage(imageUri!!, context)
                        Log.e(TAG,"imageNotNullimageNotNull=>$image")

                    }
                }

                sgnUpRequest[API_CONSTANTS.WEB_PARAM_FIRST_NAME] = getRequestBody(firstName)
                sgnUpRequest[API_CONSTANTS.WEB_PARAM_LAST_NAME] = getRequestBody(lastName)
                sgnUpRequest[API_CONSTANTS.WEB_PARAM_EMAIL] = getRequestBody(emailAddress)
                sgnUpRequest[API_CONSTANTS.WEB_PARAM_MOBILE_NUMBER] = getRequestBody(phoneNumber)
                sgnUpRequest[API_CONSTANTS.WEB_PARAM_ADDRESS] = getRequestBody(address)
                sgnUpRequest[API_CONSTANTS.WEB_PARAM_GENDER] = getRequestBody(gender)
                sgnUpRequest[API_CONSTANTS.WEB_PARAM_PASSWORD] = getRequestBody(password)
                sgnUpRequest[API_CONSTANTS.WEB_PARAM_SERVICE_ID] = getRequestBody(service_id)
                sgnUpRequest[API_CONSTANTS.WEB_PARAM_PLAN_ID] = getRequestBody(plan_id)
                sgnUpRequest[API_CONSTANTS.WEB_PARAM_DEVICE_TYPE] = getRequestBody("0")
                sgnUpRequest[API_CONSTANTS.WEB_PARAM_DEVICE_TOKEN] = getRequestBody(fcm_device_token)

                viewModelScope.launch {
                    signUpResponseObservable.value = Resource.loading(null)
                    withContext(Dispatchers.IO) {
                        response = networkService.register(sgnUpRequest, image)
                    }

                    withContext(Dispatchers.Main) {
                        response.run {
                            signUpResponseObservable.value = baseDataSource.getResult { this }
                        }
                    }
                }

            }
        } else {
            viewModelScope.launch {
                withContext(Dispatchers.Main) {
                    signUpResponseObservable.value = Resource.noInternetConnection(null)
                }
            }
        }
    }


    fun getRequestBody(content: String): RequestBody {
        return RequestBody.create("multipart/form-data".toMediaTypeOrNull(), content)
    }

    fun getMultipartImage(imagePath: Uri, context: Context): MultipartBody.Part? {

        val documentImage = File(getRealPath(context, imagePath))

        if (imageUri != null) {

            val requestImageFile = RequestBody.create(
                "multipart/form-data".toMediaTypeOrNull(),
                documentImage
            )

            return MultipartBody.Part.createFormData(
                WEB_PARAM_PROFILE_IMAGE,
                documentImage.name,
                requestImageFile
            )

        }
        return null
    }

    fun validateSignUp(): Boolean {
        Log.e("ganderVal", "gender = $gender")

        when {
            firstName.isEmpty() -> {
                sendError(ValidationStatus.EMPTY_FIRSTNAME)
            }
            !firstName.isNameValid() -> {
                sendError(ValidationStatus.TWOCHAR_FIRSTNAME)
            }
            lastName.isEmpty() -> {
                sendError(ValidationStatus.EMPTY_LASTNAME)
            }
            !lastName.isNameValid() -> {
                sendError(ValidationStatus.TWOCHAR_LASTNAME)
            }
            emailAddress.isEmpty() -> {
                sendError(ValidationStatus.EMPTYEMAIL)
            }
            !emailAddress.isEmailValid() -> {
                sendError(ValidationStatus.InvalidEmail)
            }
            phoneNumber.isEmpty() -> {
                sendError(ValidationStatus.PHONENUMBER)
            }
            !phoneNumber.isPhoneValid() -> {
                sendError(ValidationStatus.PHONENUMBERValid)
            }
            address.isEmpty()->{
                sendError(ValidationStatus.ADDRESS_EMPTY)
            }
            gender.isEmpty() -> {
                sendError(ValidationStatus.GENDERVALIDATE)
            }
            password.isEmpty() -> {
                sendError(ValidationStatus.PASSWORD)
            }
            !password.ispasswordValid() -> {
                sendError(ValidationStatus.PASSWORDVALIDATE)
            }
            !password.isSpacePassword() -> {
                sendError(ValidationStatus.SPACE_PASSWORD)
            }

            else -> {
                return true
            }
        }
        return false
    }

    fun sendError(error: ValidationStatus) {
        getValidationStatus().postValue(error)
    }

}